package com.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SalesInfoGrid {
	private double num;
	public static String TMP_PATH = "/tmp/";
	public static String LOG_FILE = "";

	/** List of recent quotes in total and by customer */
	public String getSalesInfoRecentQuotes(String param, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// System.out.println("mnthBackDt " + mnthBackDt);

		String slp = (getSlp(userId)).trim();

		String query = "select orh_sld_cus_id,cus_cus_long_nm,  COUNT(*)"
				+ " from ortorh_rec, tcttsa_rec,arrcus_rec  where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and cus_cus_id=orh_sld_cus_id and  orh_ord_pfx='QT'  and tsa_sts_typ='C'  and tsa_ref_itm=0 and tsa_ref_sbitm=0 ";

		if (slsPerson.trim().length() > 0) {
			query += " and orh_is_slp= '" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and orh_is_slp= '" + slp + "' ";
		}

		if (param.length() > 3) {
			String dtArr[] = param.split("/");
			query += " and to_char (tsa_crtd_dtts, '%Y-%m-%d') >='" + dtArr[0] + "'"
					+ " and to_char (tsa_crtd_dtts, '%Y-%m-%d') <= '" + dtArr[1] + "' ";
		} else {
			String[] mnthBackDt = getMnthDtFrmToday().split("/");
			query += " and to_char (tsa_crtd_dtts, '%Y-%m-%d') >='" + mnthBackDt[0] + "' "
					+ " and to_char (tsa_crtd_dtts, '%Y-%m-%d') <= '" + mnthBackDt[1] + "' ";
		}

		query += " GROUP BY orh_sld_cus_id,cus_cus_long_nm order by 3 desc";

		// System.out.println("recent quote : " + query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				String customer = rs.getString(1) + " - " + rs.getString(2);
				jsonObj.addProperty("customer", customer);
				jsonObj.addProperty("total", rs.getString(3));
				jsonArr.add(jsonObj);
			}

			mainObj.add("salesInfoRecentQuotesList", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** For updating widget for selected widget */
	public String updWdgtSel(String userLoginId, String widgetList) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String[] widgetArr = widgetList.split(",");

		try {
			con = DBConnection.getDBConnection();

			/** It will update status = false for all widget */
			String updateQuery1 = "update  slsdsh_wdgtDtls set status=? where USR_LGN_ID=?";
			ps = con.prepareStatement(updateQuery1);
			ps.setInt(1, 0);
			ps.setString(2, userLoginId);
			ps.executeUpdate();
			ps.close();

			if (ps != null) {
				ps.close();
				ps = null;
			}

			String updateQuery2 = "update  slsdsh_wdgtDtls set status=? where USR_LGN_ID=? and WDGT_ID=? ";
			ps = con.prepareStatement(updateQuery2);
			if (widgetArr.length > 0) {
				for (String widgetId : widgetArr) {
					// System.out.println(widgetId);
					ps.setInt(1, 1);
					ps.setString(2, userLoginId);
					ps.setInt(3, Integer.parseInt(widgetId));
					ps.executeUpdate();
				}
			}

			if (ps != null) {
				ps.close();
				ps = null;
			}

			String query3 = "select wdgtDtls.WDGT_ID,WDGT_DESC,status from slsdsh_wdgtDtls as wdgtDtls,slsdsh_wdgt wdgt where wdgtDtls.WDGT_ID=wdgt.WDGT_ID and USR_LGN_ID=?";
			// System.out.println("Query3 = "+query3);
			ps = con.prepareStatement(query3);
			ps.setString(1, userLoginId);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("widgetId", rs.getInt(1));
				jsonObj.addProperty("widgetDesc", rs.getString(2));
				jsonObj.addProperty("status", rs.getInt(3));
				jsonArr.add(jsonObj);
			}
			mainObj.add("updWgtLst", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return "false";
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * It will inserting all widget with default value if user new and return all
	 * widget list by loginId from table
	 */
	public String getWidgetListByUser(String loginId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = DBConnection.getDBConnection();
			String query1 = "select distinct USR_LGN_ID from slsdsh_wdgtDtls  where USR_LGN_ID= ?";
			// System.out.println(query1);
			ps = con.prepareStatement(query1);
			ps.setString(1, loginId);
			rs = ps.executeQuery();

			/** if user is new then by default inserting all widget with status 1 */
			if (rs.next() == false) {
				/** for inserting all 18 widgets using status true for new user */
				for (int i = 1; i <= 18; i++) {
					String insertQuery2 = "insert into slsdsh_wdgtDtls(USR_LGN_ID,WDGT_ID,status) values(?,?,?)";
					ps = con.prepareStatement(insertQuery2);
					ps.setString(1, loginId);
					ps.setInt(2, i);
					ps.setInt(3, 1);
					ps.executeUpdate();
				}
			}

			ps.close();
			rs.close();

			/** this query will run for picking all widgets for that user using login id */
			String query2 = "select wdgtDtls.WDGT_ID,WDGT_DESC,status from slsdsh_wdgtDtls as wdgtDtls,slsdsh_wdgt wdgt where wdgtDtls.WDGT_ID=wdgt.WDGT_ID and USR_LGN_ID=?";
			// System.out.println("Query2 = "+query2);
			ps = con.prepareStatement(query2);
			ps.setString(1, loginId);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("widgetId", rs.getInt(1));
				jsonObj.addProperty("widgetDesc", rs.getString(2));
				jsonObj.addProperty("status", rs.getInt(3));
				jsonArr.add(jsonObj);
			}
			mainObj.add("widgetListByUser", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** For adding user Notes */
	public String addUserNote(String loginId, String notes) {
		Connection con = null;
		PreparedStatement ps = null;
		// ,USR_NOTE_DT
		String query = "insert into slsdsh_usr_notes(USR_LGN_ID,USR_NOTES,USR_NOTE_DT) values(?,?,?)";
		// System.out.println(query);

		Date date = new Date();
		// getTime() returns current time in milliseconds
		long time = date.getTime();
		// Passed the milliseconds to constructor of Timestamp class
		Timestamp ts = new Timestamp(time);
		try {
			if (!notes.equals("")) {
				con = DBConnection.getDBConnection();
				ps = con.prepareStatement(query);
				ps.setString(1, loginId);
				ps.setString(2, notes);
				ps.setTimestamp(3, ts);
				ps.executeUpdate();
			}

			return "true";

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** For delete user Notes */
	public String deleteUserNote(String loginId, String noteId) {
		Connection con = null;
		PreparedStatement ps = null;
		// ,USR_NOTE_DT
		String query = "delete from slsdsh_usr_notes where USR_LGN_ID=? and Note_ID=?;";
		// System.out.println(query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, loginId);
			ps.setString(2, noteId);
			ps.executeUpdate();

			return "true";

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** For picking user Notes */
	public String getUserNotes() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select USR_LGN_ID,USR_NOTES,Note_ID, to_Char(USR_NOTE_DT,'%m/%d/%Y') from slsdsh_usr_notes order by USR_NOTE_DT desc";
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				// jsonObj.addProperty("lgnId", rs.getString(1));
				jsonObj.addProperty("notes", rs.getString(2));
				jsonObj.addProperty("noteId", rs.getString(3));
				jsonObj.addProperty("usr", rs.getString(4) + "-(" + rs.getString(1) + ")");
				jsonObj.addProperty("usrId", rs.getString(1));
				jsonArr.add(jsonObj);
			}

			mainObj.add("userNotesList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Last Paid Freight Information */
	public String getLstPaidFrght(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;

		String slp = (getSlp(userId)).trim();

		String query = "select stn.stn_transp_no, stn.cus_nm, stn.stn_upd_cy, stn.stn_upd_mth, "
				+ "stn.stn_upd_dy, sum(csi.val)  from (select distinct stn_transp_no, "
				+ "(trim(stn_sld_cus_id)|| '-' || trim(cus_cus_long_nm)) cus_nm, stn_upd_cy, stn_upd_mth, "
				+ "stn_upd_dy,stn_cmpy_id, stn_shpt_pfx, stn_shpt_no  from sahstn_rec, "
				+ "arrcus_rec where stn_cmpy_id = cus_cmpy_id and stn_sld_cus_id = cus_cus_id "
				+ "and stn_tot_repl_val <> 0 and stn_transp_no <> 0 ";

		int flag = 0;
		if (slsPerson.trim().length() > 0) {
			flag = 1;
			query += "and stn_is_slp= '" + slsPerson + "') stn, ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			flag = 1;
			query += "and stn_is_slp= '" + slp + "') stn, ";
		}

		if (flag == 0) {
			query += " ) stn, ";
		}

		query += "(select csi_cmpy_id, csi_ref_pfx, csi_ref_no, csi_bas_cry_val val from cttcsi_rec where csi_cst_no = 3 "
				+ "and csi_cst_cl = 'I') csi where stn.stn_cmpy_id = csi.csi_cmpy_id and stn.stn_shpt_pfx = csi.csi_ref_pfx and stn.stn_shpt_no = csi.csi_ref_no "
				+ "group by 1,2,3,4,5  order by stn.stn_upd_cy desc, stn.stn_upd_mth desc, "
				+ "stn.stn_upd_dy desc limit 10";

		// System.out.println("lst paid frgt :" + query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			resultSet = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (resultSet.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("cus", resultSet.getString(2));
				jsonObj.addProperty("trnNo", "TR-" + resultSet.getString(1));

				double extFrgt = (resultSet.getString(6) == null) ? 0
						: Math.round(Double.parseDouble(resultSet.getString(6)));

				jsonObj.addProperty("extFrght", NumberFormat.getNumberInstance(Locale.US).format(extFrgt));
				jsonArr.add(jsonObj);
			}

			// System.out.println(jsonArr);
			mainObj.add("lstPaidFrght", jsonArr);
			return mainObj.toString();

		} catch (

		SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Open AR */
	public String getOpenAr(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		/** Get Current date in YYYY-MM-dd format */
		SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
		Date date = new Date();
		String currentDate = dateFormat.format(date);

		String query = "select rvh_sld_cus_id,cus_cus_nm, to_Char(min(rvh_due_dt),'%Y-%m-%d'),\r\n"
				+ " sum(rvh_balamt) from artrvh_rec,arrcus_rec,  \r\n"
				+ " (select distinct stn_cmpy_id,stn_is_slp,stn_upd_ref  \r\n" + " from sahstn_rec ) stn  \r\n"
				+ " where  rvh_balamt<> 0 and rvh_cmpy_id=stn.stn_cmpy_id and rvh_cmpy_id=cus_cmpy_id and rvh_sld_cus_id=cus_cus_id\r\n"
				+ " and  rvh_upd_ref=stn.stn_upd_ref";

		if (slsPerson.trim().length() > 0) {
			query += " and stn.stn_is_slp = '" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and stn.stn_is_slp = '" + slp + "' ";
		}

		query += " group by  1,2  order by  4 desc";

		// System.out.println("Open ar : " + query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("customer", rs.getString(1).trim() + "-" + rs.getString(2).trim());

				String dueDate = rs.getString(3);

				LocalDate dateBefore = LocalDate.parse(dueDate);
				LocalDate dateAfter = LocalDate.parse(currentDate);

				long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);

				jsonObj.addProperty("daysPastDue", noOfDaysBetween);

				double num = Math.round(Double.parseDouble(rs.getString(4)));
				jsonObj.addProperty("amount", NumberFormat.getNumberInstance(Locale.US).format(num));
				jsonArr.add(jsonObj);
			}
			mainObj.add("openArList", jsonArr);
			return mainObj.toString();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Open Orders */
	public String getOpenOrd(String param, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// String mnthBackDt =getMonthBackDate();

		String slp = (getSlp(userId)).trim();

		String query = "select (trim(cus_cus_long_nm) ||'-'|| trim(orh_sld_cus_id)),orh_ord_no , oit_orig_ord_val  from ortorh_rec, tcttsa_rec,arrcus_rec, ortoit_rec    where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and cus_cus_id= orh_sld_cus_id and orh_ord_pfx= oit_ref_pfx and orh_ord_no = oit_ref_no and oit_ref_itm=0 and  orh_ord_pfx='SO'  and tsa_sts_actn<>'C' and tsa_sts_typ= 'T' and tsa_ref_itm=0 and tsa_ref_sbitm =0 ";

		if (slsPerson.trim().length() > 0) {
			query += " and orh_is_slp= '" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and orh_is_slp= '" + slp + "' ";
		}

		query += " order by oit_orig_ord_val desc, tsa_crtd_dtts desc";

		// System.out.println("Open ord: "+query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("customer", rs.getString(1));
				jsonObj.addProperty("ordNo", "SO-" + rs.getString(2));
				double num = Math.round(Double.parseDouble(rs.getString(3)));
				jsonObj.addProperty("amount", NumberFormat.getNumberInstance(Locale.US).format(num));

				jsonArr.add(jsonObj);
			}

			mainObj.add("openOrdLst", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Sales by Form */
	public String getSalesByForm(String[] ar, String param, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String slp = (getSlp(userId)).trim();

		try {
			con = DBConnection.getDBConnection();

			String query = "select round(sum(stn_tot_val),2)totSales FROM sahstn_rec Where ";

			if (param.length() > 3) {
				String dtArr[] = param.split("/");
				query += " to_char (stn_inv_dt, '%Y-%m-%d') > '" + dtArr[0] + "'and to_char (stn_inv_dt, '%Y-%m-%d')< '"
						+ dtArr[1] + "' ";
			} else {
				query += " to_char (stn_inv_dt, '%Y-%m-%d')> '" + ar[0] + "' and to_char (stn_inv_dt, '%Y-%m-%d')< '"
						+ ar[1] + "' ";
			}

			if (slsPerson.trim().length() > 0) {
				query += " and stn_is_slp='" + slsPerson + "' ";
			} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
				query += " and stn_is_slp='" + slp + "' ";
			}

			// System.out.println("Sales by Form for total "+query);

			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			double val = 0;
			if (rs.next()) {
				val = rs.getDouble(1);
			}

			ps.close();
			rs.close();

			query = "select stn_frm, frm_desc25 , round(sum(stn_tot_val),2)Sales FROM sahstn_rec, inrfrm_rec  WHERE stn_frm= frm_frm  ";

			if (slsPerson.trim().length() > 0) {
				query += " and stn_is_slp='" + slsPerson + "' ";
			} else if (slp.trim().length() > 0) {
				query += "and stn_is_slp='" + slp + "' ";
			}

			if (param.length() > 3) {
				String dtArr[] = param.split("/");
				query += "and to_char (stn_inv_dt, '%Y-%m-%d')> '" + dtArr[0]
						+ "'and to_char (stn_inv_dt, '%Y-%m-%d')< '" + dtArr[1] + "' ";
			} else {
				query += "and to_char (stn_inv_dt, '%Y-%m-%d')> '" + ar[0] + "' and to_char (stn_inv_dt, '%Y-%m-%d')< '"
						+ ar[1] + "' ";
			}

			query += "group by 1,2 order by 3 desc ";

			// System.out.println("Sales by Form "+query);

			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				String customer = rs.getString(1) + " - " + rs.getString(2);
				jsonObj.addProperty("customer", customer);
				num = Math.round(Double.parseDouble(rs.getString(3)));
				jsonObj.addProperty("value", NumberFormat.getNumberInstance(Locale.US).format(num));
				String percent = String.format("%.2f", (num / val) * 100);
				if (percent.equals("0.00") || percent.equals("-0.00")) {
					percent = "0";
				}
				jsonObj.addProperty("percent", percent);

				jsonArr.add(jsonObj);
			}

			mainObj.add("salesByFormList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Sales by Customer */
	public String getSalesByCustomer(String[] ar, String param, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		String query = "SELECT (stn_sld_cus_id || '-'||stn_nm1), round(sum(stn_tot_val),2)Sales FROM sahstn_rec  where  ";

		if (slsPerson.trim().length() > 0) {
			query += " stn_is_slp='" + slsPerson + "' and ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " stn_is_slp='" + slp + "' and ";
		}

		if (param.length() > 3) {
			String dtArr[] = param.split("/");
			query += " to_char (stn_inv_dt, '%Y-%m-%d')> '" + dtArr[0] + "' and to_char (stn_inv_dt, '%Y-%m-%d')< '"
					+ dtArr[1] + "' ";
		} else {
			query += " to_char (stn_inv_dt, '%Y-%m-%d')> '" + ar[0] + "' and to_char (stn_inv_dt, '%Y-%m-%d')< '"
					+ ar[1] + "' ";
		}
		query += "group by 1 order by 2 desc ";

		// System.out.println("sales By Customer "+query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("customer", rs.getString(1));
				num = Math.round(Double.parseDouble(rs.getString(2)));
				jsonObj.addProperty("value", NumberFormat.getNumberInstance(Locale.US).format(num));

				jsonArr.add(jsonObj);
			}

			mainObj.add("salesByCusList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Average Days to Pay */
	public String getAvgDaysToPay(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		String query = "select (trim(cub_cus_id)||'-'||trim(cus_cus_long_nm)),  cub_apd_1,cub_apd_2,cub_apd_3,cub_apd_4,cub_apd_5,cub_apd_6,cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12 from arbcub_rec,arrcus_rec ";

		if (slsPerson.trim().length() > 0) {
			query += ",arrshp_rec ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += ",arrshp_rec ";
		}

		query += "where cub_cus_id=cus_cus_id ";

		if (slsPerson.trim().length() > 0) {
			query += "and cus_cus_id= shp_cus_id and shp_is_slp= '" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += "and cus_cus_id= shp_cus_id and shp_is_slp= '" + slp + "'";
		}

		// System.out.println("Avg day to pay : " + query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			ArrayList<AvgDayToPay> avgDayList = new ArrayList<>();

			while (rs.next()) {
				double count6 = 0, count12 = 0, sum6 = 0, sum12 = 0;
				for (int i = 2; i <= 13; i++) {
					int temp = Integer.parseInt((rs.getString(i) != null ? rs.getString(i) : "0"));
					if (temp != 0.0) {
						if (i > 6) {
							count6++;
							sum6 += temp;
						}
						count12++;
						sum12 += temp;
					}
				}

				// System.out.println("sum12 and count12 "+sum12+" "+count12);
				// System.out.println("sum6 and count6 "+sum6+" "+count6);
				double avg6 = 0, avg12 = 0;
				if (count6 != 0.0) {
					avg6 = sum6 / count6; // for 6 month
				}
				if (count12 != 0.0) {
					avg12 = sum12 / count12; // for 12 month
				}

				AvgDayToPay avgDay = new AvgDayToPay(rs.getString(1), avg6, avg12);
				avgDayList.add(avgDay);
			}

			Collections.sort(avgDayList); // sorted here
			// System.out.println("avgDayList :: "+avgDayList);
			for (int i = 0; i < avgDayList.size(); i++) {
				AvgDayToPay avgDay = avgDayList.get(i);
				jsonObj = new JsonObject();
				jsonObj.addProperty("cusId", avgDay.getCusId());
				jsonObj.addProperty("avg6", Math.round(avgDay.getAvg6()));
				jsonObj.addProperty("avg12", Math.round(avgDay.getAvg12()));
				jsonArr.add(jsonObj);
			}

			mainObj.add("avgDaysToPayList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Last quote and last activity */
	public String getSalesInfoLastQuotes(String[] ar, String param, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		String query = "select (trim(orh_sld_cus_id) ||'-'||trim(cus_cus_long_nm)), orh_ord_no, to_char(tsa_lst_upd_dtts, '%m/%d/%Y')tsa_lst_upd_dtts from ortorh_rec ,arrcus_rec,tcttsa_rec "
				+ " where orh_cmpy_id= tsa_cmpy_id and orh_cmpy_id=cus_cmpy_id and orh_sld_cus_id=cus_cus_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and tsa_ref_itm=0 and tsa_ref_sbitm=0 and orh_ord_pfx='QT' and tsa_sts_typ='T' and tsa_sts_actn<>'C' ";

		if (param.length() > 3) {
			String dtArr[] = param.split("/");
			query += "  and to_Char(tsa_lst_upd_dtts,'%Y-%m-%d') >='" + dtArr[0]
					+ "' and  to_Char(tsa_lst_upd_dtts,'%Y-%m-%d') <='" + dtArr[1] + "'";
		} else {
			query += "  and to_Char(tsa_lst_upd_dtts,'%Y-%m-%d') >='" + ar[0]
					+ "' and  to_Char(tsa_lst_upd_dtts,'%Y-%m-%d') <='" + ar[1] + "'";
		}

		if (slsPerson.trim().length() > 0) {
			query += " and orh_is_slp='" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and orh_is_slp='" + slp + "' ";
		}

		query += " ORDER BY tsa_lst_upd_dtts desc ";

		// System.out.println("Lst qt lst act :: > " + query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("cusId", rs.getString(1));
				jsonObj.addProperty("ordNo", "QT-" + rs.getString(2));
				jsonObj.addProperty("date", rs.getString(3));
				jsonArr.add(jsonObj);
			}

			mainObj.add("salesInfoLastQuotesList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Quote By Date Range */
	public String getQuoteByDateRange(String param, String[] ar, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();
		int flag = 0;

		String query = "select distinct(ord_ord_no),to_Char(ord_cus_po_dt,'%m/%d/%Y') , (trim(ord_sld_cus_id)||'-'|| trim(cus_cus_long_nm) ),ord_cus_po_dt from ";

		if (slsPerson.trim().length() > 0 || slp.trim().length() > 0) {
			flag = 1;
			query += " ortorh_rec , ";
		}
		query += " ortord_rec ,arrcus_rec ";

		if (flag == 1) {
			query += " where orh_cmpy_id = ord_cmpy_id and orh_ord_pfx= ord_ord_pfx and orh_ord_no = ord_ord_no and orh_cmpy_id=cus_cmpy_id and orh_sld_cus_id=cus_cus_id ";
			if (slsPerson.trim().length() > 0) {
				query += " and orh_is_slp = '" + slsPerson + "'";
			} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
				query += "  and orh_is_slp = '" + slp + "'";
			}
		} else {
			query += " where ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id ";
		}

		if (param.length() > 3) {
			String dtArr[] = param.split("/");
			query += " and to_char (ord_cus_po_dt, '%Y-%m-%d') >='" + dtArr[0]
					+ "' and to_char (ord_cus_po_dt, '%Y-%m-%d') <='" + dtArr[1] + "' ";
		} else {
			query += " and to_char (ord_cus_po_dt, '%Y-%m-%d') >='" + ar[0]
					+ "' and to_char (ord_cus_po_dt, '%Y-%m-%d') <='" + ar[1] + "' ";
		}

		if (flag == 1) {
			query += " and orh_ord_pfx='QT' ";
		} else {
			query += " and ord_ord_pfx='QT' ";
		}

		query += "  and ord_cus_po_dt is not null  order by ord_cus_po_dt desc ";

		// System.out.println("Qt By DT= " + query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);

			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("ordNo", "QT-" + rs.getString(1));
				jsonObj.addProperty("date", rs.getString(2));
				jsonObj.addProperty("customer", rs.getString(3));
				jsonArr.add(jsonObj);
			}

			mainObj.add("qtByDtRangeLst", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** Last sales by customer */
	public String getLstSlsByCus(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();
		String query = "select distinct to_Char(coc_lst_sls_dt,'%m/%d/%Y') ,coc_lst_pmt_amt ,(trim(coc_cus_id)||'-'||trim(cus_cus_long_nm)),coc_lst_sls_dt  from arbcoc_rec, arrcus_rec ";

		if (slsPerson.trim().length() > 0) {
			query += " ,arrshp_rec ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " ,arrshp_rec ";
		}

		query += " where coc_cmpy_id = cus_cmpy_id and coc_cus_id=cus_cus_id";

		if (slsPerson.trim().length() > 0) {
			query += " and cus_cmpy_id = shp_cmpy_id and cus_cus_id = shp_cus_id and shp_is_slp='" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and cus_cmpy_id = shp_cmpy_id and cus_cus_id = shp_cus_id and shp_is_slp= '" + slp + "'";
		}

		query += " and coc_lst_pmt_amt <> 0 order by coc_lst_sls_dt desc";

		// System.out.println("Last sls By Cust : " + query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				if (rs.getString(1) != null) {
					jsonObj = new JsonObject();
					jsonObj.addProperty("date", rs.getString(1));
					num = Math.round(Double.parseDouble((rs.getString(2) != null ? rs.getString(2) : "0")));
					jsonObj.addProperty("lastSales", NumberFormat.getNumberInstance(Locale.US).format(num));
					jsonObj.addProperty("customer", rs.getString(3));
					jsonArr.add(jsonObj);
				}
			}
			mainObj.add("lstSlsByCusList", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Late Invoices */
	public String getLateInvoice(String param, String[] ar, String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		String query = "select (trim(rvh_sld_cus_id)||'-'||trim(cus_cus_long_nm))cus,rvh_upd_ref, "
				+ " to_Char(rvh_due_dt,'%m/%d/%Y'),rvh_due_dt from artrvh_rec ,arrcus_rec,arrshp_rec "
				+ " where rvh_cmpy_id = cus_cmpy_id and rvh_sld_cus_id = cus_cus_id and shp_cmpy_id = rvh_cmpy_id  "
				+ " and shp_cus_id = rvh_cr_ctl_cus_id and rvh_balamt > 0 and shp_shp_to = 0 ";

		if (slsPerson.trim().length() > 0) {
			query += " and shp_is_slp = '" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += "and shp_is_slp = '" + slp + "'";
		}

		query += " and rvh_due_dt < today order by rvh_due_dt desc ";

		// System.out.println("Late Inv :" + query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("customer", rs.getString(1));
				jsonObj.addProperty("invNo", rs.getString(2));
				jsonObj.addProperty("dueDt", rs.getString(3));
				jsonArr.add(jsonObj);
			}

			mainObj.add("lateInvLst", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getCrYearProfit(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		Date date1 = c.getTime();
		String[] dtArr = dateFormat.format(date1).split("-");
		int crYear = Integer.parseInt(dtArr[0]);

		String query = "SELECT year(stn_inv_dt),cus_cus_long_nm,stn_is_slp, sum(stn_tot_val) Sales,"
				+ "  round((sum(stn_tot_val)-sum(stn_tot_avg_val)),2) gross_profit FROM sahstn_rec, "
				+ " arrcus_rec WHERE cus_cus_id=stn_sld_cus_id and year(stn_inv_dt)=" + crYear;

		if (slsPerson.trim().length() > 0) {
			query += " and stn_is_slp= '" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and stn_is_slp=  '" + slp + "'";
		}

		query += " GROUP BY 1,2,3 ORDER BY 4 desc ";

		// System.out.println("CrYearProfit :: " + query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("customer", rs.getString(2));
				double num1 = Math.round(Double.parseDouble((rs.getString(4) != null ? rs.getString(4) : "0")));
				jsonObj.addProperty("sales", NumberFormat.getNumberInstance(Locale.US).format(num1));
				double num2 = Math.round(Double.parseDouble((rs.getString(5) != null ? rs.getString(5) : "0")));
				jsonObj.addProperty("profit", NumberFormat.getNumberInstance(Locale.US).format(num2));
				jsonArr.add(jsonObj);
			}

			mainObj.add("crYrPrft", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getPvYearProfit(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (getSlp(userId)).trim();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		Date date1 = c.getTime();
		String[] dtArr = dateFormat.format(date1).split("-");
		int pvYear = Integer.parseInt(dtArr[0]) - 1;

		String query = "SELECT year(stn_inv_dt),cus_cus_long_nm,stn_is_slp, sum(stn_tot_val) Sales,"
				+ "  round((sum(stn_tot_val)-sum(stn_tot_avg_val)),2) gross_profit FROM sahstn_rec, "
				+ " arrcus_rec WHERE cus_cus_id=stn_sld_cus_id and year(stn_inv_dt)=" + pvYear;

		if (slsPerson.trim().length() > 0) {
			query += " and stn_is_slp= '" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and stn_is_slp=  '" + slp + "'";
		}

		query += " GROUP BY 1,2,3 ORDER BY 4 desc ";

		// System.out.println("pvYearProfit :: " + query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("customer", rs.getString(2));
				double num1 = Math.round(Double.parseDouble((rs.getString(4) != null ? rs.getString(4) : "0")));
				jsonObj.addProperty("sales", NumberFormat.getNumberInstance(Locale.US).format(num1));
				double num2 = Math.round(Double.parseDouble((rs.getString(5) != null ? rs.getString(5) : "0")));
				jsonObj.addProperty("profit", NumberFormat.getNumberInstance(Locale.US).format(num2));
				jsonArr.add(jsonObj);
			}

			mainObj.add("pvYrPrft", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getCrAndPvYearSmry(String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		String[] ar = getCurrentDate().split("-");

		int crYear = Integer.parseInt(ar[0]), crMonth = 12;
		int pvYear = crYear - 1, pvMonth = Integer.parseInt(ar[1]);
		if (pvMonth == 12) {
			pvMonth = 1;
		} else {
			pvMonth = pvMonth + 1;
		}

		/*
		 * if (iGlbMonth == 12) { iGlbMonth = 1; iGlbYear = iGlbYear; } else { iGlbMonth
		 * = iGlbMonth + 1; iGlbYear = iGlbYear - 1; }
		 */

		JsonObject mainObj = new JsonObject();
		JsonArray jsonArr = new JsonArray();

		try {
			con = DBConnection.getDBConnection();

			jsonArr.add(getSmryByYear(slsPerson, userId, slpFilter, crMonth, crYear, "C", con));
			jsonArr.add(getSmryByYear(slsPerson, userId, slpFilter, pvMonth, pvYear, "P", con));

			mainObj.add("crAndPvYrSmry", jsonArr);
		} finally {

			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return mainObj.toString();
	}

	public JsonObject getSmryByYear(String slsPerson, String userId, String slpFilter, int month, int year, String flag,
			Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		JsonObject jsonObj = new JsonObject();
		String slp = (getSlp(userId)).trim();

		try {

			String query = "SELECT stn_is_slp, COUNT(distinct stn_ord_no) invoiced_order_count,year(stn_inv_dt),"
					+ " round(sum(stn_tot_val), 0) Total_Revenue,round(sum(stn_tot_val) / " + month
					+ ", 0) Monthly_Revenue, "
					+ " round((sum(stn_tot_val) - sum(stn_tot_avg_val)), 2) total_gross_profit,round((sum(stn_tot_val) - sum(stn_tot_avg_val)) / "
					+ month + ", 2) Monthly_Gross_Profit  FROM sahstn_rec WHERE 1 = 1	AND year(stn_inv_dt)= " + year
					+ " and stn_inv_dt is not null ";

			if (slsPerson.trim().length() > 0) {
				query += " and stn_is_slp= '" + slsPerson + "'";
			} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
				query += " and stn_is_slp=  '" + slp + "'";
			}

			query += " group by stn_is_slp,3 having sum(stn_tot_val)<>0 order by 2,1";

			// System.out.println("getSmryByYear :: " + query);

			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			if (flag.equals("C")) {
				jsonObj.addProperty("year", "YTD " + year);
			} else {
				jsonObj.addProperty("year", "FY " + year);
			}

			if (rs.next()) {
				String invOrd = checkNull(rs.getString(2));
				String totRev = checkNull(rs.getString(4));
				double avgOrd = 0;

				if (invOrd.length() > 0 && !invOrd.equals("0") && totRev.length() > 0) {
					avgOrd = Double.parseDouble(totRev) / Double.parseDouble(invOrd);
				}
				jsonObj.addProperty("avgOrd", commaSeparator("" + avgOrd));
				jsonObj.addProperty("invOrd", invOrd);
				jsonObj.addProperty("totRev", commaSeparator("" + totRev));
				jsonObj.addProperty("monRev", commaSeparator(checkNull(rs.getString(5))));
				jsonObj.addProperty("totGrssPrft", commaSeparator(checkNull(rs.getString(6))));
				jsonObj.addProperty("monGrssPrft", commaSeparator(checkNull(rs.getString(7))));
			} else {
				jsonObj.addProperty("avgOrd", "NA");
				jsonObj.addProperty("invOrd", "NA");
				jsonObj.addProperty("totRev", "NA");
				jsonObj.addProperty("monRev", "NA");
				jsonObj.addProperty("totGrssPrft", "NA");
				jsonObj.addProperty("monGrssPrft", "NA");
			}

			// ***********************************************************************************************

			rs.close();
			ps.close();
			ps = null;
			rs = null;

			query = "select count(distinct cus_cus_long_nm)prospects,year(crd_acct_opn_dt),"
					+ " shp_is_slp	from arrcrd_rec,	arrcus_rec,	arrshp_rec	where	"
					+ " shp_cmpy_id=cus_cmpy_id and	shp_cus_id=cus_cus_id  and	"
					+ " crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and year(crd_acct_opn_dt)= " + year;

			if (slsPerson.trim().length() > 0) {
				query += " and shp_is_slp= '" + slsPerson + "'";
			} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
				query += " and shp_is_slp=  '" + slp + "'";
			}
			query += "group by 2,3 order by 2,1 ";

			// System.out.println("getSmryByYear :: " + query);

			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			if (rs.next()) {
				String prospect = checkNull(rs.getString(1));
				jsonObj.addProperty("prospect", (prospect.length() == 0 ? "0" : prospect));
			} else {
				jsonObj.addProperty("prospect", "NA");
			}

			// ***********************************************************************************************

			rs.close();
			ps.close();
			ps = null;
			rs = null;

			query = "select count(distinct ord_ord_no)quotes,year(ord_cus_po_dt),shp_is_slp "
					+ "from ortord_rec ,arrcus_rec,arrshp_rec where shp_cmpy_id=cus_cmpy_id "
					+ " and shp_cus_id=cus_cus_id  and ord_cmpy_id=cus_cmpy_id and "
					+ " ord_sld_cus_id=cus_cus_id AND year(ord_cus_po_dt) = " + year + " and ord_ord_pfx='QT' "
					+ " and ord_cus_po_dt is not null ";

			if (slsPerson.trim().length() > 0) {
				query += " and shp_is_slp= '" + slsPerson + "'";
			} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
				query += " and shp_is_slp=  '" + slp + "'";
			}
			query += "group by 2,3 order by 2,1 ";

			// System.out.println("getSmryByYear :: " + query);

			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			if (rs.next()) {
				String quotes = checkNull(rs.getString(1));
				jsonObj.addProperty("quote", commaSeparator(quotes));
			} else {
				jsonObj.addProperty("quote", "NA");
			}

			// ***********************************************************************************************

			rs.close();
			ps.close();
			ps = null;
			rs = null;

			query = "select count(distinct cus_cus_id)	from arrcrd_rec,arrcus_rec,	"
					+ "arrshp_rec where	shp_cmpy_id=cus_cmpy_id and	shp_cus_id=cus_cus_id  "
					+ "and	crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and year(crd_co_lim_eff_dt)=" + year;

			if (slsPerson.trim().length() > 0) {
				query += " and shp_is_slp= '" + slsPerson + "'";
			} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
				query += " and shp_is_slp=  '" + slp + "'";
			}

			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			if (rs.next()) {
				String quotes = checkNull(rs.getString(1));
				jsonObj.addProperty("cusSold", commaSeparator(quotes));
			} else {
				jsonObj.addProperty("cusSold", "NA");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		return jsonObj;
	}

	/** For getting rolling 12 month from date to toDate */
	public static String[] getRolling12MnthOld() {

		/** for calculating rolling 12 month frmDate to toDate */
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		// System.out.println("Current Date " +currentDate );
		String ar1[] = currentDate.split("-");

		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		String toDateTmp = dateFormat.format(lastDateOfPreviousMonth);
		String ar2[] = toDateTmp.split("-");

		String frmDate = "" + (Integer.parseInt(ar1[0]) - 1) + "-" + ar2[1] + "-" + "01";
		String toDate = ar2[0] + "-" + ar1[1] + "-" + ar2[2];

		// System.out.println("frmDate " +frmDate );
		// System.out.println("toDate "+toDate);

		String[] ar = new String[2];
		ar[0] = frmDate;
		ar[1] = toDate;

		return ar;
	}

	/** For getting rolling 12 month from date to toDate */
	public static String[] getRolling12Mnth() {

		/** for calculating rolling 12 month frmDate to toDate */
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar aCalendar = Calendar.getInstance();

		// Get the last date of the previous month
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		String toDate = dateFormat.format(lastDateOfPreviousMonth);

		// Get the first date of 12 months ago
		aCalendar.add(Calendar.MONTH, -11);
		aCalendar.set(Calendar.DATE, 1);
		Date firstDateOf12MonthsAgo = aCalendar.getTime();
		String frmDate = dateFormat.format(firstDateOf12MonthsAgo);

		//System.out.println("frmDate " + frmDate);
		//System.out.println("toDate " + toDate);
		
		String[] ar = new String[2];
		ar[0] = frmDate;
		ar[1] = toDate;

		return ar;
		
	}

	/** For get sales person List By login id */
	public String getSlsList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "select slp_slp,slp_lgn_id from scrslp_rec where slp_actv=1 order by slp_lgn_id";
		// System.out.println(query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("slsId", rs.getString(1));
				jsonObj.addProperty("slsNm", rs.getString(2));
				jsonArr.add(jsonObj);
			}

			mainObj.add("slsList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** For get sales person By login id */
	public String getSlp(String userId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String query = "select slp_slp from scrslp_rec where slp_actv=1 and slp_lgn_id='" + userId + "'";
		// System.out.println(query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			String slp = "";
			if (rs.next()) {
				slp = rs.getString(1);
			}
			return slp;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	/** for Matching that loginId is Admin or not */
	public String isLgnIdMatched(String loginId) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String isMatched = "false";

		try {
			String query = "select lgn_id from slsdsh_usrDtls where lgn_id='" + loginId.trim() + "' and usr_role='A' ";
			// System.out.println(query);

			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				isMatched = "true";
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return isMatched;
	}

	/** Month back date from currect date in format yyyy-MM-dd */
	public String getMonthBackDt() {
		// for taking date format
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		// Convert Date to Calendar
		Calendar c = Calendar.getInstance();

		// taking 1 month back date
		c.add(Calendar.MONTH, -1);

		// Convert calendar back to Date*/
		Date date1 = c.getTime();
		String mnthBackDt = dateFormat.format(date1);
		return mnthBackDt;
	}

	/** Format Date yyyy-MM-dd **/
	public String getMnthDtFrmToday() {
		// for taking date format
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String toDate = dateFormat.format(date);
		// System.out.println(toDate);
		// Convert Date to Calendar
		Calendar c = Calendar.getInstance();

		// taking 1 month back date
		c.add(Calendar.MONTH, -1);

		// Convert calendar back to Date*/
		Date date1 = c.getTime();
		String fromDate = dateFormat.format(date1);
		// System.out.println(toDate);
		// System.out.println(fromDate + "/" + toDate);
		return fromDate + "/" + toDate;
	}

	/** Date Format MM/dd/yyyy */
	public String getMnthDtFrmToday2() {
		// for taking date format
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String toDate = dateFormat.format(date);
		// System.out.println(toDate);
		// Convert Date to Calendar
		Calendar c = Calendar.getInstance();

		// taking 1 month back date
		c.add(Calendar.MONTH, -1);

		// Convert calendar back to Date*/
		Date date1 = c.getTime();
		String fromDate = dateFormat.format(date1);
		// System.out.println(fromDate);

		// System.out.println(fromDate + "-" + toDate);

		return fromDate + "-" + toDate;
	}

	public String getCurrentDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		Date date1 = c.getTime();
		String dtStr = dateFormat.format(date1);
		return dtStr;
	}

	public String checkNull(String val) {
		return (val != null ? val.trim() : "");
	}

	public String commaSeparator(String val) {

		if (val.trim().length() == 0 || val.trim().equals("0")) {
			val = "NA";
		}

		if (isNumber(val)) {
			return val;
		} else {
			double num = Math.round(Double.parseDouble(val));
			return NumberFormat.getNumberInstance(Locale.US).format(num);
		}
	}

	public boolean isNumber(String val) {
		// LogMessage.writeLogs("In isNumber");
		boolean flag = false;
		val = val.toUpperCase();
		for (int i = 0; i < val.length(); i++) {
			char temp = val.charAt(i);
			if (temp >= 'A' && temp <= 'Z') {
				flag = true;
				break;
			}
		}
		// LogMessage.writeLogs("In isNumber "+flag);
		return flag;
	}

}
