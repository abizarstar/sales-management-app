package com.util;

public class Rolling12 {
	private int year;
	private int month;
	
	public Rolling12(int year, int month) {
		super();
		this.year = year;
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}
	
	 @Override
	    public int hashCode() {
	      //System.out.println("In hashcode");
	      return this.getMonth();
	    }
	
	@Override
    public boolean equals(Object obj) {
		Rolling12 r = null;
	    if(obj instanceof Rolling12){
	        r = (Rolling12) obj;
	    }
	    //System.out.println("In equals");
	    if(this.month == r.getMonth()){
	        return true;
	    } else {
	        return false;
	    }  
     }

}
