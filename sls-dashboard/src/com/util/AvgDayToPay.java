package com.util;

public class AvgDayToPay implements Comparable<AvgDayToPay> {
	private String cusId;
	private double avg6;
	private double avg12;

	public AvgDayToPay(String cusId, double avg6, double avg12) {
		super();
		this.cusId = cusId;
		this.avg6 = avg6;
		this.avg12 = avg12;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public double getAvg6() {
		return avg6;
	}

	public void setAvg6(long avg6) {
		this.avg6 = avg6;
	}

	public double getAvg12() {
		return avg12;
	}

	public void setAvg12(long avg12) {
		this.avg12 = avg12;
	}

	@Override
	public int compareTo(AvgDayToPay o) {
		if (o == null) {
			return -1;
		}
		int c = Double.valueOf(o.getAvg6()).compareTo(this.avg6); //Double.valueOf(avg6)
		return c;
	}

	@Override
	public String toString() {
		return "AvgDayToPay [cusId=" + cusId + ", avg6=" + avg6 + ", avg12=" + avg12 + "]";
	}

}
