package com.util;

import java.sql.Connection;
import java.sql.DriverManager;


public class DBConnection {

	public static Connection getDBConnection() {

		String URL = "";
		String dbUsername = "";
		String dbPassword = "";

		try {
			if (System.getenv("SERVER") != null) {
				if (System.getenv("SERVER").equals("POSTGRES")) {
					Class.forName("org.postgresql.Driver");
					String ipAddr = System.getenv("PGHOST");
					String port = System.getenv("PGPORT");
					String db = System.getenv("PGDATABASE");

					URL = "jdbc:postgresql://" + ipAddr + ":" + port + "/" + db;

					dbUsername = "postgres";
					dbPassword = "postgres";

				} else if (System.getenv("SERVER").equals("INFORMIX")) {

					Class.forName("com.informix.jdbc.IfxDriver");
					String ipAddr = System.getenv("PRI_SRVR");
					String port = System.getenv("DBPORT");
					String db = System.getenv("DATABASE");
					String server = System.getenv("INFORMIXSERVER");

					URL = "jdbc:informix-sqli://" + ipAddr + ":" + port + "/" + db + ":INFORMIXSERVER=" + server;

					dbUsername = System.getenv("USER");
					dbPassword = System.getenv("USER");

					// dbUsername = "starsoft"; dbPassword = "vDvP5e4N";

					// Remove comment while installing on livapp
					dbUsername = "livappod";
					dbPassword = "livappod";

					// for tstapp

					// dbUsername = "tstappod"; dbPassword = "tstappod";

				}
			} else {

				Class.forName("com.informix.jdbc.IfxDriver");

				String ipAddr = "172.20.100.70";
				String port = "17005";

				String db = "ap11pr03";
				String server = "saturnapspr12;DB_LOCALE=en_us.8859-1";

				URL = "jdbc:informix-sqli://" + ipAddr + ":" + port + "/" + db + ":INFORMIXSERVER=" + server;

				// System.out.println("URL = "+URL);
				dbUsername = "ap1103";
				dbPassword = "ap1103";
			}

			String dbConnectionURL = URL;
			Connection connection = DriverManager.getConnection(dbConnectionURL, dbUsername, dbPassword);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
			return connection;
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			} else {
				throw new RuntimeException(e);
			}
		}
	}

}
