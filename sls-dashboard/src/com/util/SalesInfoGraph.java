package com.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SalesInfoGraph {

	SalesInfoGrid salesInfoGrid = new SalesInfoGrid();

	/** No of new customer for Rolling 12 Month */
	public String getNewCustomerByMonth(String[] ar, LinkedHashMap<Rolling12, Integer> r, String param,
			String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (salesInfoGrid.getSlp(userId)).trim();

		String query = "select month(crd_acct_opn_dt)mon, year(crd_acct_opn_dt)yr, count(distinct cus_cus_long_nm) from  arrcus_rec , arrcrd_rec ";

		if (slsPerson.trim().length() > 0 || slp.trim().length() > 0) {
			query += ",arrshp_rec ";
		}

		query += " where  crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id ";

		if (slsPerson.trim().length() > 0) {
			query += "and cus_cus_id= shp_cus_id and shp_is_slp= '" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += "and cus_cus_id= shp_cus_id and shp_is_slp= '" + slp + "' ";
		}

		/*
		 * if (param.length() > 3) { String dtArr[] = param.split("/"); query +=
		 * "and crd_acct_opn_dt>= '" + dtArr[0] + "' and crd_acct_opn_dt<= '" + dtArr[1]
		 * + "' "; } else {
		 */

		query += "and to_char(crd_acct_opn_dt, '%Y-%m-%d') >= '" + ar[0]
				+ "' and to_char(crd_acct_opn_dt, '%Y-%m-%d')<= '" + ar[1] + "' ";

		query += "and cus_cus_acct_typ<>'P' group by 1,2 order by 2 asc,1 desc";

		System.out.println("New Customer :: "+query);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(2), rs.getInt(1)), rs.getInt(3));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+" "
				// +entry.getValue());
			}
			mainObj.add("newCustomerByMonth", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** No of new Prospect customer Rolling 12 month */
	public String getNewPropectsCusByMonth(String[] ar, LinkedHashMap<Rolling12, Integer> r, String param,
			String slsPerson, String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (salesInfoGrid.getSlp(userId)).trim();

		String query = "select month(crd_acct_opn_dt)mon, year(crd_acct_opn_dt)yr, count(distinct cus_cus_long_nm) from  arrcus_rec , arrcrd_rec ";

		if (slsPerson.trim().length() > 0 || slp.trim().length() > 0) {
			query += ",arrshp_rec ";
		}
		query += "  where crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id ";

		if (slsPerson.trim().length() > 0) {
			query += " and cus_cus_id= shp_cus_id and shp_is_slp= '" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += "and cus_cus_id= shp_cus_id and shp_is_slp= '" + slp + "'";
		}

		/*
		 * if (param.length() > 3) { String dtArr[] = param.split("/"); query +=
		 * " and crd_acct_opn_dt>='" + dtArr[0] + "' and  crd_acct_opn_dt<= '" +
		 * dtArr[1] + "' "; } else {
		 */

		query += " and to_char(crd_acct_opn_dt, '%Y-%m-%d')>='" + ar[0]
				+ "' and  to_char(crd_acct_opn_dt, '%Y-%m-%d')<= '" + ar[1] + "' ";

		query += "and cus_cus_acct_typ='P' group by 1,2 order by 2 asc,1 asc";

		 System.out.println("new Prospect cus = "+query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(2), rs.getInt(1)), rs.getInt(3));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+" "
				// +entry.getValue());
			}

			mainObj.add("newPropectsCusByMonth", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Quote VS Orders */
	public String getQuoteVsOrd(String[] ar, LinkedHashMap<Rolling12, Integer> r, String param, String slsPerson,
			String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String slp = (salesInfoGrid.getSlp(userId)).trim();
		// System.out.println("slp :: "+slp);

		String query1 = "select  count(distinct ord_ord_no), month(ord_cus_po_dt) mon , year(ord_cus_po_dt) yr from ";

		if (slsPerson.trim().length() > 0 || slp.trim().length() > 0) {
			query1 += "ortorh_rec, ortord_rec ,arrcus_rec where  orh_cmpy_id = ord_cmpy_id and orh_ord_pfx= ord_ord_pfx and orh_ord_no = ord_ord_no and orh_cmpy_id=cus_cmpy_id and orh_sld_cus_id=cus_cus_id and ord_ord_pfx='QT'  and ord_cus_po_dt is not null ";
		} else {
			query1 += " ortord_rec ,arrcus_rec where ord_ord_pfx='QT' and ord_cus_po_dt is not null ";
		}

		String query2 = "select  count(distinct stn_ord_no), month(stn_cus_po_dt)mon , year(stn_cus_po_dt)yr from sahstn_rec where stn_ord_pfx='SO' and stn_cus_po_dt is not null  ";

		if (slsPerson.trim().length() > 0) {
			query1 += " and orh_is_slp = '" + slsPerson + "' ";
			query2 += " and stn_is_slp= '" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query1 += " and orh_is_slp = '" + slp + "' ";
			query2 += " and stn_is_slp= '" + slp + "' ";
		}

		query1 += " and to_char(ord_cus_po_dt, '%Y-%m-%d')>= '" + ar[0] + "' and to_char(ord_cus_po_dt, '%Y-%m-%d') <='"
				+ ar[1] + "' ";
		query2 += " and to_char(stn_cus_po_dt, '%Y-%m-%d')>= '" + ar[0] + "' and to_char(stn_cus_po_dt, '%Y-%m-%d') <='"
				+ ar[1] + "' ";

		query1 += " group by 2,3   order by 3 asc ,2  asc";
		query2 += " group by 2,3   order by 3 asc ,2  asc";

		 System.out.println("Quote : "+query1);
		 System.out.println("Order : "+query2);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query1);
			rs = ps.executeQuery();

			/*
			 * ps2=con.prepareStatement(query2); ResultSet rs2=ps2.executeQuery();
			 */

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr1 = new JsonArray();
			JsonArray jsonArr2 = new JsonArray();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(3), rs.getInt(2)), rs.getInt(1));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr1.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+" "
				// +entry.getValue());
			}

			ps.close();
			rs.close();

			r = getRollMnYear();
			ps = con.prepareStatement(query2);
			rs = ps.executeQuery();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(3), rs.getInt(2)), rs.getInt(1));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr2.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+"
				// " +entry.getValue());
			}

			jsonObj = new JsonObject();
			jsonObj.add("quoteList", jsonArr1);
			jsonObj.add("orderList", jsonArr2);

			mainObj.add("qtVsOrdLst", jsonObj);
			// System.out.println(mainObj);
			// System.out.println(jsonArr2);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Quote VS Orders Revenue */
	public String getQuoteVsOrdRev(String[] ar, LinkedHashMap<Rolling12, Integer> r, String param, String slsPerson,
			String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (salesInfoGrid.getSlp(userId)).trim();

		String query1 = "select sum(oit_orig_ord_val), month(tsa_crtd_dtts)mon , year(tsa_crtd_dtts)yr  from ortorh_rec,ortoit_rec,tcttsa_rec where orh_cmpy_id = oit_cmpy_id and orh_ord_pfx= oit_ref_pfx and orh_ord_no=oit_ref_no and oit_ref_itm=0 and tsa_sts_typ='T' and orh_cmpy_id=tsa_cmpy_id and orh_ord_pfx = tsa_ref_pfx and orh_ord_no=tsa_ref_no and oit_ref_itm = tsa_ref_itm ";
		String query2 = "select sum(stn_tot_val), month(stn_cus_po_dt)mon , year(stn_cus_po_dt)yr from sahstn_rec where stn_ord_pfx='SO' and stn_cus_po_dt is not null  ";

		if (slsPerson.trim().length() > 0) {
			query2 += " and  stn_is_slp= '" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query2 += " and  stn_is_slp= '" + slp + "' ";
		}

		/*
		 * if (param.length() > 3) { String dtArr[] = param.split("/"); query1 +=
		 * " and  date(tsa_crtd_dtts)>= '" + dtArr[0] + "' and date(tsa_crtd_dtts) <='"
		 * + dtArr[1] + "'"; query2 += " and stn_cus_po_dt>= '" + dtArr[0] +
		 * "'and stn_cus_po_dt <='" + dtArr[1] + "' "; } else {
		 */
		query1 += " and  to_char(tsa_crtd_dtts, '%Y-%m-%d') >= '" + ar[0]
				+ "' and  to_char(tsa_crtd_dtts, '%Y-%m-%d')  <='" + ar[1] + "'";
		query2 += " and  to_char(stn_cus_po_dt, '%Y-%m-%d')  >= '" + ar[0]
				+ "'and to_char(stn_cus_po_dt, '%Y-%m-%d') <='" + ar[1] + "' ";

		if (slsPerson.trim().length() > 0) {
			query1 += " and  orh_is_slp='" + slsPerson + "' ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query1 += " and  orh_is_slp= '" + slp + "' ";
		}

		query1 += " and orh_ord_pfx='QT'  group by 2,3 order by 3 asc ,2  asc";
		query2 += " group by 2,3   order by 3 asc ,2  asc";

		 System.out.println("Quote : " + query1);
		 System.out.println("Orders Revenue : " + query2);
		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query1);
			rs = ps.executeQuery();

			/*
			 * ps2=con.prepareStatement(query2); ResultSet rs2=ps2.executeQuery();
			 */

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr1 = new JsonArray();
			JsonArray jsonArr2 = new JsonArray();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(3), rs.getInt(2)), rs.getInt(1));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr1.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+"
				// " +entry.getValue());
			}

			ps.close();
			rs.close();

			r = getRollMnYear();
			ps = con.prepareStatement(query2);
			rs = ps.executeQuery();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(3), rs.getInt(2)), rs.getInt(1));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr2.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+" "
				// +entry.getValue());
			}
			jsonObj = new JsonObject();
			jsonObj.add("quoteList", jsonArr1);
			jsonObj.add("orderRevList", jsonArr2);

			mainObj.add("qtVsOrdRevLst", jsonObj);
			// System.out.println(mainObj);
			// System.out.println(jsonArr2);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
				} catch (Exception e) {
				}
			}
		}
	}

	/** GP Earned by Month Rolling 12 month */
	public String getGpErndByMonth(String[] ar, LinkedHashMap<Rolling12, Integer> r, String param, String slsPerson,
			String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (salesInfoGrid.getSlp(userId)).trim();

		String query = "select month(stn_inv_dt) ,year(stn_inv_dt), round((sum(stn_tot_val)-sum(stn_tot_avg_val)),2) gross_profit FROM sahstn_rec where   ";

		if (slsPerson.trim().length() > 0) {
			query += "stn_is_slp= '" + slsPerson + "' and ";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += "stn_is_slp= '" + slp + "' and ";
		}

		/*
		 * if (param.length() > 3) { String dtArr[] = param.split("/"); query +=
		 * " stn_inv_dt> '" + dtArr[0] + "' and stn_inv_dt< '" + dtArr[1] + "' "; }
		 * else{
		 */
		query += " to_char(stn_inv_dt, '%Y-%m-%d') > '" + ar[0] + "' and to_char(stn_inv_dt, '%Y-%m-%d')< '" + ar[1]
				+ "' ";

		query += " and stn_inv_dt is not null group by 1,2 order by 2 asc , 1 asc";

		 System.out.println("Gp Earned "+query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(2), rs.getInt(1)), rs.getInt(3));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("gp", entry.getValue());
				jsonArr.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+"
				// " +entry.getValue());
			}
			mainObj.add("gpErndByMnthList", jsonArr);
			// System.out.println(jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Quotes by Prospects */
	public String getQtByPros(String[] ar, LinkedHashMap<Rolling12, Integer> r, String param, String slsPerson,
			String userId, String slpFilter) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String slp = (salesInfoGrid.getSlp(userId)).trim();

		String query = "select month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr ,count(distinct ord_ord_no) from ortord_rec ,arrcus_rec,arrcrd_rec ";

		if (slsPerson.trim().length() > 0 || slp.trim().length() > 0) {
			query += " ,arrshp_rec ";
		}

		query += " where ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id and crd_cus_id=cus_cus_id ";

		if (slsPerson.trim().length() > 0) {
			query += " and cus_cus_id= shp_cus_id and shp_is_slp= '" + slsPerson + "'";
		} else if (slp.trim().length() > 0) { // && slpFilter.equalsIgnoreCase("yes")
			query += " and cus_cus_id= shp_cus_id and shp_is_slp= '" + slp + "'";
		}

		query += " and ord_ord_pfx='QT' and cus_cus_acct_typ='P' and ord_cus_po_dt is not null ";

		/*
		 * if (param.length() > 3) { String dtArr[] = param.split("/"); query +=
		 * " and ord_cus_po_dt>='" + dtArr[0] + "' and ord_cus_po_dt <='" + dtArr[1] +
		 * "' "; } else {
		 */

		query += " and to_char(ord_cus_po_dt, '%Y-%m-%d') >='" + ar[0] + "' and to_char(ord_cus_po_dt, '%Y-%m-%d') <='"
				+ ar[1] + "' ";

		query += " group by 1,2 order by 2 asc ,1 asc";

		 System.out.println("Qt By Prs : "+query);

		try {
			con = DBConnection.getDBConnection();
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			JsonObject mainObj = new JsonObject();
			JsonObject jsonObj = null;
			JsonArray jsonArr = new JsonArray();

			while (rs.next()) {
				r.replace(new Rolling12(rs.getInt(2), rs.getInt(1)), rs.getInt(3));
			}
			for (Map.Entry<Rolling12, Integer> entry : r.entrySet()) {
				jsonObj = new JsonObject();
				jsonObj.addProperty("month", entry.getKey().getMonth());
				jsonObj.addProperty("year", entry.getKey().getYear());
				jsonObj.addProperty("count", entry.getValue());
				jsonArr.add(jsonObj);
				// System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()+"
				// " +entry.getValue());
			}
			mainObj.add("qtByPrsList", jsonArr);
			return mainObj.toString();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public LinkedHashMap<Rolling12, Integer> getRollMnYear() {
		/** for calculating rolling 12 month frmDate to toDate */
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		// System.out.println("Current Date " +currentDate );
		String ar1[] = currentDate.split("-");
		String frmDate = "" + (Integer.parseInt(ar1[0]) - 1) + "-" + ar1[1];
		// System.out.println("frmDate " +frmDate );

		Calendar aCalendar = Calendar.getInstance();
		// aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -1);
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		String toDate = dateFormat.format(lastDateOfPreviousMonth);
		String ar2[] = toDate.split("-");
		// System.out.println("toDate "+toDate);

		// System.out.println(Month.of(11).name().substring(0,3));

		// String
		// date1=Month.of(Integer.parseInt(ar1[1])).name().substring(0,3)+"-"+(Integer.parseInt(ar1[0])-1);
		// String
		// date2=Month.of(Integer.parseInt(ar2[1])+1).name().substring(0,3)+"-"+(Integer.parseInt(ar1[0]));

		DateFormat formater = new SimpleDateFormat("yyyy-MM");

		Calendar beginCalendar = Calendar.getInstance();
		Calendar finishCalendar = Calendar.getInstance();

		try {
			beginCalendar.setTime(formater.parse(frmDate));
			finishCalendar.setTime(formater.parse(toDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		LinkedHashMap<Rolling12, Integer> roll12 = new LinkedHashMap<>();

		while (beginCalendar.before(finishCalendar)) {
			String dateFinal = formater.format(beginCalendar.getTime()).toUpperCase();
			String[] ar = dateFinal.split("-");
			roll12.put(new Rolling12(Integer.parseInt(ar[0]), Integer.parseInt(ar[1])), 0);

			// System.out.println(dateFinal);

			beginCalendar.add(Calendar.MONTH, 1);
		}
		/*
		 * for (Map.Entry<Rolling12, Integer> entry : roll12.entrySet()) {
		 * //System.out.println(entry.getKey().getMonth()+"-"+entry.getKey().getYear()
		 * +" " +entry.getValue().toString()); }
		 */
		return roll12;
	}

}
