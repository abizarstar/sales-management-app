package com.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Test {

	public static void main(String[] args) {

		getRolling12Mnth();
		getRolling12Mnth2();
		

	}

	public static void getRolling12Mnth() {

		/** for calculating rolling 12 month frmDate to toDate */
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		System.out.println("Current Date " +currentDate );
		String ar1[] = currentDate.split("-");
		
		

		Calendar aCalendar = Calendar.getInstance();
		aCalendar.set(Calendar.DATE, 1);
		aCalendar.add(Calendar.DAY_OF_MONTH, -2);
		Date lastDateOfPreviousMonth = aCalendar.getTime();
		String toDateTmp = dateFormat.format(lastDateOfPreviousMonth);
		System.out.println("toDateTmp :: "+toDateTmp);
		String ar2[] = toDateTmp.split("-");

		String frmDate = "" + (Integer.parseInt(ar1[0]) - 1) + "-" + ar1[1] + "-" + "01";
		String toDate = ar2[0] + "-" + ar2[1] + "-" + ar2[2];

		System.out.println("frmDate " + frmDate);
		System.out.println("toDate " + toDate);

		String[] ar = new String[2];
		ar[0] = frmDate;
		ar[1] = toDate;

		//return ar;
	}
	
	public static void getRolling12Mnth2() {
		
        /** for calculating rolling 12 month frmDate to toDate */
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar aCalendar = Calendar.getInstance();

        // Get the last date of the previous month
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.add(Calendar.DAY_OF_MONTH, -1);
        Date lastDateOfPreviousMonth = aCalendar.getTime();
        String toDate = dateFormat.format(lastDateOfPreviousMonth);

        // Get the first date of 12 months ago
        aCalendar.add(Calendar.MONTH, -11);
        aCalendar.set(Calendar.DATE, 1);
        Date firstDateOf12MonthsAgo = aCalendar.getTime();
        String frmDate = dateFormat.format(firstDateOf12MonthsAgo);

        System.out.println("frmDate " + frmDate);
        System.out.println("toDate " + toDate);
    }
	
	
	
	
	
	
	
	
	/** Format Date yyyy-MM-dd **/
	public static String getMnthDtFrmToday() {
		// for taking date format
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String toDate = dateFormat.format(date);
		System.out.println(toDate);
		// Convert Date to Calendar
		Calendar c = Calendar.getInstance();

		// taking 1 month back date
		c.add(Calendar.MONTH, -1);

		// Convert calendar back to Date*/
		Date date1 = c.getTime();
		String fromDate = dateFormat.format(date1);
		System.out.println(toDate);
		System.out.println(fromDate + "/" + toDate);
		return fromDate + "/" + toDate;
	}

	/** Date Format MM/dd/yyyy */
	public static String getMnthDtFrmToday2() {
		// for taking date format
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String toDate = dateFormat.format(date);
		// System.out.println(toDate);
		// Convert Date to Calendar
		Calendar c = Calendar.getInstance();

		// taking 1 month back date
		c.add(Calendar.MONTH, -1);

		// Convert calendar back to Date*/
		Date date1 = c.getTime();
		String fromDate = dateFormat.format(date1);
		// System.out.println(fromDate);

		System.out.println(fromDate + "-" + toDate);

		return fromDate + "-" + toDate;

	}

}
