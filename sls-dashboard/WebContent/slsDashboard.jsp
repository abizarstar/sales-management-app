<%@page import="com.util.GetAuthCode"%>
<%@page import="java.util.List"%>
<%@ page import="sun.misc.BASE64Decoder"%>
<%@ page import="sun.misc.BASE64Encoder"%>
<%@ page import="javax.crypto.Cipher"%>
<%@ page import="javax.crypto.spec.SecretKeySpec"%>
<%@ page import="javax.crypto.spec.IvParameterSpec"%>
<%@ page import="javax.crypto.BadPaddingException"%>
<%@ page import="javax.crypto.IllegalBlockSizeException"%>
<%@ page import="java.security.AlgorithmParameters"%>
<%@ page import="java.security.Key"%>
<%@ page import="java.io.IOException"%>
<%@ page import="java.util.StringTokenizer"%>
<%@page import="com.google.gson.*"%>
<%@page import="com.util.DBConnection"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.util.Rolling12"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.util.SalesInfoGraph"%>
<%@page import="com.util.SalesInfoGrid"%>




<%
	String outputVal = "";

	if (request.getMethod().equals("POST")) {

		String param = request.getParameter("param");
		String execFlg = request.getParameter("execFlg");

		if (execFlg.equals("I")) {

			String paramRequest = "";
			String path = "";
			String STARBIN = "/u/starsoft/bin/";
			String execPath = "";
			String arguements = "";
			int checkStatus = 0;
			String paramUserName = "";
			String paramPassword = "";
			String INSLOG = "B";
			String requestType = "";
			String applicationName = "";

			paramUserName = request.getParameter("user");
			paramPassword = request.getParameter("pass");

			execPath = STARBIN + "customerlogin";
			arguements = paramUserName + "|" + paramPassword;

			ProcessBuilder pb = new ProcessBuilder(execPath, arguements);
			Process pr = pb.start();
			checkStatus = pr.waitFor();
			//checkStatus = 0;
			outputVal = String.valueOf(checkStatus);

		} else {
			String usrId = request.getParameter("usrId");
			String slsPerson = request.getParameter("slsPer");
			String slpFilter = request.getParameter("slpFilter");
			SalesInfoGrid salesInfoGrid = new SalesInfoGrid();
			SalesInfoGraph salesInfoGraph = new SalesInfoGraph();

			/** calling this method for getting rolling 12 month, from date - toDate */
			String[] ar = SalesInfoGrid.getRolling12Mnth();

			/** getting month with year after calling this method */
			LinkedHashMap<Rolling12, Integer> r = salesInfoGraph.getRollMnYear();

			if (execFlg.equals("1")) {
				outputVal = salesInfoGrid.getSalesInfoRecentQuotes(param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("2")) {
				outputVal = salesInfoGrid.getUserNotes();
			} else if (execFlg.equals("3")) {
				outputVal = salesInfoGrid.getOpenOrd(param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("4")) {
				outputVal = salesInfoGrid.getLstPaidFrght(slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("5")) {
				outputVal = salesInfoGrid.getOpenAr(slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("6")) {
				outputVal = salesInfoGraph.getNewCustomerByMonth(ar, r, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("7")) {
				outputVal = salesInfoGraph.getNewPropectsCusByMonth(ar, r, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("8")) {
				outputVal = salesInfoGraph.getQuoteVsOrd(ar, r, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("9")) {
				outputVal = salesInfoGraph.getQuoteVsOrdRev(ar, r, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("10")) {
				outputVal = salesInfoGraph.getGpErndByMonth(ar, r, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("11")) {
				outputVal = salesInfoGraph.getQtByPros(ar, r, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("12")) {
				outputVal = salesInfoGrid.getSalesByForm(ar, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("13")) {
				outputVal = salesInfoGrid.getSalesByCustomer(ar, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("14")) {
				outputVal = salesInfoGrid.getAvgDaysToPay(slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("15")) {
				outputVal = salesInfoGrid.getSalesInfoLastQuotes(ar, param, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("16")) {
				outputVal = salesInfoGrid.getQuoteByDateRange(param, ar, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("17")) {
				outputVal = salesInfoGrid.getLstSlsByCus(slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("18")) {
				outputVal = salesInfoGrid.getLateInvoice(param, ar, slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("20")) {
				outputVal = salesInfoGrid.getCrYearProfit(slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("21")) {
				outputVal = salesInfoGrid.getPvYearProfit(slsPerson, usrId, slpFilter);
			} else if (execFlg.equals("22")) {
				outputVal = salesInfoGrid.getCrAndPvYearSmry(slsPerson, usrId, slpFilter);
				System.out.println("outputVal >> " + outputVal);
			}

			else if (execFlg.equals("A")) {
				// if we get empty string, in this case we will get Number format exception in method that's why adding here invalid value in param
				if (param.equals("")) {
					param = "-1";
				}
				outputVal = salesInfoGrid.updWdgtSel(usrId, param);
			} else if (execFlg.equals("B")) {
				outputVal = salesInfoGrid.addUserNote(usrId, param);
			} else if (execFlg.equals("C")) {
				outputVal = salesInfoGrid.getWidgetListByUser(usrId);
			} else if (execFlg.equals("D")) {
				outputVal = salesInfoGrid.deleteUserNote(usrId, param);
			} else if (execFlg.equals("AA")) {
				outputVal = salesInfoGrid.isLgnIdMatched(usrId);
			} else if (execFlg.equals("AB")) {
				outputVal = salesInfoGrid.getSlsList();
			} else if (execFlg.equals("MnthBackDt")) {
				outputVal = salesInfoGrid.getMnthDtFrmToday2();
			} else {
				outputVal = "";
			}

		}

		//System.out.println("IN POST, Parameters -->" + param);
	}
%>

<%=outputVal%>
