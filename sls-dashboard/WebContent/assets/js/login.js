
var dbUrl = 'slsDashboard.jsp';
$(function(){
	
	
	$("#loginError").hide();
	
	var urlParams = new URLSearchParams(window.location.search);
	
	console.log(urlParams.has('param'))
	
	if(urlParams.has('param'))
	{
		var inpParam = urlParams.get('param');
		
		var url = window.location.origin;
		
		var urlArr = url.split("//");
		
		callLogin(inpParam, urlArr[1]);
	}

    
    $("#password").keyup(function(event){
    	if (event.keyCode === 13) {
    		$("#btnLogin").trigger("click");
    	}
    });
    
    
	$("#btnLogin").click(function(){
	
		$("#mainLoader").show();
		//$("#btnLogin").hide();
		$("#loadingLoginWrapper").show();
		
		$("#formLogin .form-control").removeClass("border-danger");
		$("#loginError").html("");
		
		var error = false;
		
		var userId = $("#userId").val();
		var password = $("#password").val();
		
		if( userId == undefined || userId == "" || userId.trim().length == 0 ){
			error = true;
			$("#userId").addClass("border-danger");
		}
		
		if( password == undefined || password == "" || password.trim().length == 0 ){
			error = true;
			$("#password").addClass("border-danger");
		}
		
		
		if( error == false ){
		
			getUserDetails();
			
		}else{
			
			var errList = '<ul class="m-3 pl-1">';
			errList += '<li><h5>'+'Please enter User Id/Password' +'</h5></li>';					
			errList += '</ul>';
			customAlertTop(errList, "bg-warning");
			$("#mainLoader").hide();
			$("#loadingLoginWrapper").hide();
		    $("#btnLogin").show();
		    
		}
	});
	
});


function callLogin(inpParam, appUrl)
{
	$("#mainLoader").show();
	
	$.ajax({
				type: 'POST',
				dataType: 'json',
		        url: dbUrl,
		        data: {
					execFlg: "I",
					param: inpParam,
					appUrl: appUrl,
				},
		        success: function(response, textStatus, jqXHR) {
		           
		           console.log(response);
		            console.log(textStatus);
		            console.log(jqXHR.status);
		            
		          if( response.rtnSts == 1)
		            {
		            	if( response.msgList != undefined && response.msgList != undefined ){
		            		var loopLimit = response.msgList.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-3 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li><h5>'+ response.msgList[i].msg +'</h5></li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	customAlertTop(errList);
		         
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            	$("#mainLoader").hide();
		            }
		            else
		            {		            
		            	localStorage.setItem("starAppSlsAuthToken", response.authToken);
		            	localStorage.setItem("starAppSlsUserId", response.userId);
		            	localStorage.setItem("starAppSlsUserNm", response.userNm);
		            	localStorage.setItem("starAppSlsCmpy", response.cmpy);
		            	localStorage.setItem("starAppSlsAppHost",response.server);
		            	localStorage.setItem("starAppSlsAppPort", response.port);
		            	localStorage.setItem("starAppSlsCtlNo", response.qdsCtlNo);
		            	localStorage.setItem("starAppSlsBaseLng", response.baseLanguage);  
		            	localStorage.setItem("starAppSlsBaseCtry", response.baseCountry);             	
		        
		            	
		            	//redirectToDashboard();
		            	
		            	//window.location.replace("upld-invc.html");
		            	window.location.replace("index.html");
		            	
		            	$("#mainLoader").hide();
		            }
		           }
		    });
}

function getUserDetails(application){
		
       $("#mainLoader").show();
        var userId=$("#userId").val();
        var pwd=$("#password").val();
           $.ajax( /**Getting Existing Form values**/ {
        data: {
            execFlg: "I",
            user:userId,
            pass:pwd
        },
        url: dbUrl,
        type: 'POST',
        dataType: 'text',
        success: function(flag) {
             console.log("flag value = "+flag);
                $("#loading").hide();
                if(flag==0)/**Flag 2-> Success, Any Other Flag -> User ID/Password not correct**/
                        {


                                //$("#loginForm").submit();
                                localStorage.setItem("starAppSlsUserId", userId);
                                window.location.replace("index.html");


                        }else if(flag==3)
                        {
                                //$("#loginForm").submit();
                                localStorage.setItem("starAppSlsUserId", userId);
                                window.location.replace("index.html");
                        }
                        else
                        {
                                var errMsg = 'Invalid User ID/Password';
								$("#loginError").html( errMsg );
								$("#loginError").show();
		            			$("#mainLoader").hide();
								$("#loadingLoginWrapper").hide();
		            			$("#btnLogin").show();
		            			

                        }
			$("#mainLoader").hide();
        },
        error: function() {
        	$("#mainLoader").hide();
           // alert('failure');
        }
                });
}

function closeAlertBox(){
window.setTimeout(function () {
  $("#msgAlertBox").fadeOut(300)
}, 3000);
}
function customAlertTop(errList, divClass){

	$(".notify").toggleClass("active-alert");		            	
	$(".notify").addClass(divClass);
	$("#notifyType").html(errList);
	
	 setTimeout(function(){
	    $(".notify").removeClass("active-alert");
	  },3000);
		         
}