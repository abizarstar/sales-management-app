/*
	Created By   :- Nawab Alam
	Created Date :- Feb 2022
	Description  :- Javascript For App Sales Dashboard
*/


var dateRange="",isMatched=false,slsPerson="",slpFilter="yes",slpDflt="";
var dbUrl = 'slsDashboard.jsp';
var myChart1, myChart2, myChart3, myChart4, myChart5, myChart6;
$(document).ready(function(){

	if(localStorage.getItem("starAppSlsUserId") != null)
	{
		$('.select2').select2();
		
		getMnthDtFrmToday2(); /** It will select date range from today to back month day for 30 days */
		
		isLgnIdMatched();
		
		getDatePicker();	
	
		/** calling this function on load for showing widgets by user */
		getWidgetListByUser();
		
		getCrAndPvYearSmry();
		getCrYearProfit();
		getPvYearProfit();
	
	}
	else{
		window.location.replace("login.html");
	}
	
	
	$("#clear").click(function(){	
		$("#slsPerson").val("").trigger("change");				
		$("#dtFltr").val("");
		dateRange="";
		slsPerson="";
		slpFilter = "no";
	});
	
	$("#signOut").click(function(){
		localStorage.removeItem("starAppSlsUserId");					
		window.location.replace("login.html");
	});
	
	
	$("#search").click(function(){
		dateRange=getDateRange();
		slpFilter = "no";
		if(isMatched==true){
			slsPerson=$("#slsPerson").val().trim();
		}
		
		//console.log('date range on click search ' + dateRange);
		/** called here for selected date range */
		getQuoteByDateRange();
		
		getCrAndPvYearSmry();
		getCrYearProfit();
		getPvYearProfit();
		
		
		/** (List of Recent Quotes in Total and By customer) */
		getSalesInfoRecentQuotesList();
		getSalesInfoLastQuotes();
		getOpenAr();
		getOpenOrd();
		getLstPaidFrght();
		getSalesByForm();
		getSalesByCustomer();
		getLstSlsByCus();
		getLateInvoice();
		getAvgDaysToPay();
		getNewCustomerByMonth();
		getNewPropectsCusByMonth();
		getQuoteVsOrd();
		getQuoteVsOrdRev();
		getGpErndByMonth();
		getQtByPros();		
	});
	
	
	 /** This method will call after pressing saveButton. */
	 $("#saveFilters").click(function(){
			//alert('On click of save filter');
			$("mainLoader").show();
			var checkBoxArr = [];
            $.each($("#filtrTbl tbody input[name='isSelected']:checked"), function(){            
                checkBoxArr.push($(this).val());
            });  
   		  /** This method will update, checked widget */  
		  updWdgtSel(checkBoxArr); 
   		  
		
	});/** end of this function */
	
	
	
	/** 1. on click refresh icon(List of Recent Quotes in Total and By customer) */
	$("#salesRecentQuotesIcon").click(function(){
		getSalesInfoRecentQuotesList();
	});
	
	/** 2. on click refresh icon (For To Do List )*/
	$("#userNotesIcon").click(function(){
		getUserNotes();
	});
	
	/** 3. on click refresh icon (For Open Orders) */
	$("#openOrdIcon").click(function(){
		getOpenOrd();
	});
	
	/** 4. on click refresh icon (Last Paid Freight Information) */
	$("#lstPaidFrtIcon").click(function(){
		getLstPaidFrght();
	});
	
	/** 5. on click refresh icon (For Open AR) */
	$("#openArIcon").click(function(){
		getOpenAr();
	});
	
	/** 6. on click refresh icon (For New Customer - By Month)*/
	$("#newCustomer").click(function(){
		getNewCustomerByMonth();
	});
	
	/** 7. on click refresh icon (For New Prospects - By Month) */
	$("#newCustomerProspects").click(function(){
		getNewPropectsCusByMonth();
	});
	
	/** 8. on click refresh icon  (For Quote vs Orders) */
	$("#qtVsOrdIcon").click(function(){
		getQuoteVsOrd();
	});
	
	/** 9. on click refresh icon (For Quote vs Orders Revenue) */
	$("#qtVsOrdRevIcon").click(function(){
		getQuoteVsOrdRev();
	});
	
	/** 10. on click refresh icon  (For GP Earned by Month) */
	$("#gpErndIcon").click(function(){
		getGpErndByMonth();
	});
	
	/** 11. on click refresh icon  (For Quotes by Prospects) */
	$("#qtByPrsIcon").click(function(){
		getQtByPros();
	});
	
	/** 12. on click refresh icon  (For Sales by Form) */
	$("#salesByFormIcon").click(function(){
		getSalesByForm();
	});
	
	/** 13. on click refresh icon  (For Sales by Customer) */
	$("#salesByCustomerIcon").click(function(){
		getSalesByCustomer();
	});
	
	/** 14. on click refresh icon  (For Average Days to Pay) */
	$("#avgDaysToPayIcon").click(function(){
		getAvgDaysToPay();
	});
	
	/** 15. on click refresh icon (For Last Quote and Activity)*/
	$("#salesLastQuotesIcon").click(function(){
		alert('On Click '+dateRange);
		getSalesInfoLastQuotes();
	});
	
	/** 16. on click refresh icon (For Quote By Date Range) */
	$("#qtByDtRangeIcon").click(function(){
		getQuoteByDateRange();
	});
	
	/** 17. on click refresh icon (For Last sales by customer) */
	$("#lstSlsByCusIcon").click(function(){
		getLstSlsByCus();
	});
	
	/** 18. on click refresh icon  (For Late Invoices) */
    $("#lateInvIcon").click(function(){
		getLateInvoice();
	});
	
	$("#crYearIcon").click(function(){
		getCrYearProfit();
	});
	
	$("#pvYearIcon").click(function(){
		getPvYearProfit();
	});
	
	$("#crAndPvYrSmryIcon").click(function(){
		getCrAndPvYearSmry();
	});
	
	
	/** calling  addNote function on click Add button */
	$("#addUserNote").click(function(){
		addUserNote();
		$("#floatingTextarea1").val("");
	});
	
	
	$("#userNotes").on('click', '.btnDelete', function (event) {
		var noteId=event.target.id;   	
    	//alert(event.target.id);
    	deleteUserNote(noteId);
   });
	
	
    
	
}); /** end of $(document).ready block */
//-------------------------------------------------------

function getCrAndPvYearSmry(){
		/** It's added here for showing loading animation */
		$("#mainLoaderCrAndPvYrSmry").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "22",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			    // console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var crAndPvYrSmry = arrResponse.crAndPvYrSmry;
					var loopLimit = crAndPvYrSmry.length;
					
					if(loopLimit==0){
						tblRows += '<tr><td class="text-left ">' + "No Records Found." + '</td><td></td><td></td></tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
						tblRows += '<tr>'+
										'<td>' 					+ crAndPvYrSmry[i].year + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].prospect + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].quote + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].invOrd + '</td>'+
										'<td class="text-left ">' + crAndPvYrSmry[i].cusSold + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].avgOrd + '</td>'+
										'<td class="text-left ">' + crAndPvYrSmry[i].totRev + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].monRev + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].totGrssPrft + '</td>'+
										'<td class="text-left">' + crAndPvYrSmry[i].monGrssPrft + '</td>'+
									'</tr>';
					}			
				}
				
				
				$("#crAndPvYrSmryDtl tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoaderCrAndPvYrSmry").hide();
			},
			error: function(data){
				$("#mainLoaderCrAndPvYrSmry").hide();
			}
		});
		
		
}/** end of getCrAndPvYearSmry() */






function getCrYearProfit(){
		/** It's added here for showing loading animation */
		$("#mainLoaderCr").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "20",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			    //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var crYrPrft = arrResponse.crYrPrft;
					var loopLimit = crYrPrft.length;
					if(loopLimit==0){
						tblRows += '<tr><td class="text-left ">' + "No Records Found." + '</td><td></td><td></td></tr>';
					}	
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr>'+
											'<td class="text-left ">' + crYrPrft[i].customer + '</td>'+
											'<td class="text-end">' + crYrPrft[i].sales + '</td>'+
											'<td class="text-end">' + crYrPrft[i].profit + '</td>'+
										'</tr>';
					}
				}
				
				var crDate = new Date().getFullYear();
				const pvDate = crDate-1;
				
				var tblHead = '<tr><th>FY '+crDate+'</th><th class="text-end">Sales($)</th>	<th class="text-end">Gross Profit($)</th></tr>';
				
				$("#crYearDtl thead").html( tblHead );
				$("#crYearDtl tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoaderCr").hide();
			},
			error: function(data){
				$("#mainLoaderCr").hide();
			}
		});
		
		
}/** end of getCrYearProfit() */


function getPvYearProfit(){
		/** It's added here for showing loading animation */
		$("#mainLoaderPv").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "21",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			    // console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var pvYrPrft = arrResponse.pvYrPrft;
					var loopLimit = pvYrPrft.length;
					if(loopLimit==0){
						tblRows += '<tr><td class="text-left ">' + "No Records Found." + '</td><td></td><td></td></tr>';
					}
					for( var i=0; i<loopLimit; i++ ){							
						tblRows += '<tr>'+
										'<td class="text-left ">' + pvYrPrft[i].customer + '</td>'+
										'<td class="text-end">' + pvYrPrft[i].sales + '</td>'+
										'<td class="text-end">' + pvYrPrft[i].profit + '</td>'+
									'</tr>';
					}
						
					
				}
				const crDate = new Date().getFullYear();
				const pvDate = crDate-1;
				var tblHead = '<tr><th>FY '+pvDate+'</th><th class="text-end">Sales($)</th><th class="text-end">Gross Profit($)</th></tr>';
				
				$("#pvYearDtl thead").html( tblHead );
				$("#pvYearDtl tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoaderPv").hide();
			},
			error: function(data){
				$("#mainLoaderPv").hide();
			}
		});
		
		
}/** end of getPvYearProfit() */


function getDatePicker(){
	if( $.isFunction( $.fn.daterangepicker ) ){
			$('.date').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				opens: 'left',
				locale: {
					format: 'MM/DD/YYYY'
				}
			});
			
			
			//$("#slsPrsDiv").addClass("d-none");
			
			$('body').on('focus',"input[name='daterange']", function(){
				$('input[name="daterange"]').daterangepicker({
					autoUpdateInput: false,
					opens: 'left',
					locale: {
						format: 'MM/DD/YYYY'
					}
				}, function(start, end, label) {
					console.log("A new date selection was made: " + start.format('MM/DD/YYYY') + ' to ' + end.format('MM/DD/YYYY'));
				});
				
				$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
					$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
					$(this).trigger("change");
				});
		
				$('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
					$(this).val('');
					$(this).trigger("change");
				});
		
			});
	}
}
//-------------------------------------------------------
function isLgnIdMatched(){
		//$("#slsPrsDiv").removeClass("d-none");						
		$.ajax({
			data: {
				execFlg: "AA",
				param: "",
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				if (IsJsonString(data.trim())){
					isMatched = JSON.parse(data.trim());
					if(isMatched==true){
						$("#slsPrsDiv").removeClass("d-none");	
						$("#editDropdown").show();
						getSlsList();
					}
					else{
						$("#slsPrsDiv").addClass("d-none");	
						$("#editDropdown").hide();	
					}
				}
				$("#mainLoader").hide();
			},
			error: function(data){
				$("#mainLoader").hide();
			}
		});
}
//------------------------------------------------------
function getSlsList(){
	//$("#mainLoader").show();
	console.log('getSlsList method called');
	$.ajax({
			data: {
				execFlg: "AB",
				param: "",
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				console.log('Slp list :: '+data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var slsList = arrResponse.slsList;
					var loopLimit = slsList.length;
									
					var slsId = "", slsNm = "", options = "<option value=''>Select Sales Person</option>";	
					for( var i=0; i<loopLimit; i++ ){
						
						slsId = slsList[i].slsId.trim();
						slsNm = slsList[i].slsNm.trim();
						
						if(localStorage.getItem("starAppSlsUserId") == slsNm){
							options += '<option value="'+ slsId +'" selected>'+ slsId + ' (' + slsNm + ')</option>';
						}else{
							options += '<option value="'+ slsId +'">'+ slsId + ' (' + slsNm + ')</option>';
						}
						
						
						// options += '<option value="'+ slsId +'">'+ slsId + ' (' + slsNm + ')</option>';
					}	
					$("#slsPerson").html( options );	
				}
					
				$("#mainLoader").hide();		
			},
			error: function(data){
				$("#mainLoader").hide();
			}
		});			
}
//----------------------------------------------------------------------------
/** 1. List of Recent Quotes in Total and By customer */
function getSalesInfoRecentQuotesList(){
		/** It's added here for showing loading animation */
		$("#mainLoader1").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "1",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			    //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var salesInfoRecentQuotesList = arrResponse.salesInfoRecentQuotesList;
					var loopLimit = salesInfoRecentQuotesList.length;
					var cusId = 0, rowClass = "";
					
					if(loopLimit==0){
						tblRows += '<tr><td class="text-left ">' + "No Records Found." + '</td><td></td><td></td></tr>';
					}	
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr>'+
											'<td class="text-left ">' + salesInfoRecentQuotesList[i].customer + '</td>'+
											'<td class="text-end">' + salesInfoRecentQuotesList[i].total + '</td>'+
										'</tr>';
					}
				}
				
				$("#salesRecentQuotes tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader1").hide();
			},
			error: function(data){
				$("#mainLoader1").hide();
			}
		});
		
		
}/** end of getSalesInfoRecentQuotesList() */

//----------------------------------------------------------
/** 2. To Do List  */
function getUserNotes(){
		/** It's added here for showing loading animation */
		$("#mainLoader2").show();
		var uId = localStorage.getItem("starAppSlsUserId");
		//console.log("uId "+ uId)
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "2",
				param: "",
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var userNotesList = arrResponse.userNotesList;
					var loopLimit = userNotesList.length;
					var cusId = 0,tmpCount=0;
					for( var i=0; i<loopLimit; i++ ){								
						if(uId.trim()==userNotesList[i].usrId.trim()){
							tmpCount = tmpCount + 1;
							tblRows=tblRows+'<tr>'+
									'<td class="text-left ">' + userNotesList[i].notes + '</td>'+
									'<td class="text-end">' + userNotesList[i].usr + '</td>'+
									'<td class="text-end"><i  id="'+userNotesList[i].noteId+'" class="align-middle fas fa-fw fa-trash text-danger btnDelete"></i></td>'+
									'</tr>';
						}					
					}			
					if(tmpCount==0){
						tblRows += '<tr><td class="text-left">' + "No Records Found." + '</td><td></td><td></td></tr>';					
					}							
				}		
				$("#userNotes tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader2").hide();
			},
			error: function(data){
				$("#mainLoader2").hide();
			}
		});
} /** end of getUserNotes() */


//------------------------------------------------------
/** 3. Open Orders */
function getOpenOrd(){
		/** It's added here for showing loading animation */
		$("#mainLoader3").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "3",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			  // //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var openOrdLst = arrResponse.openOrdLst;
					var loopLimit = openOrdLst.length;
					var cusId = 0;
					
					if(loopLimit==0){
						tblRows += '<tr><td class="text-left ">' + "No Records Found." + '</td><td></td><td></td></tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr>'+
											'<td class="text-left ">' + openOrdLst[i].ordNo + '</td>'+
											'<td class="text-left">' + openOrdLst[i].customer + '</td>'+
											'<td class="text-left">' + openOrdLst[i].amount + '</td>'+
										'</tr>';
					}
				}
				
				$("#openOrd tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader3").hide();
			},
			error: function(data){
				$("#mainLoader3").hide();
			}
		});
		   
} /** end of getOpenOrd() */
//----------------------------------------------------------
/** 4. Last Paid Freight Information */
function getLstPaidFrght(){
		/** It's added here for showing loading animation */
		$("#mainLoader4").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "4",
				param: "",
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			   //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var lstPaidFrght = arrResponse.lstPaidFrght;
					var loopLimit = lstPaidFrght.length;
					var cusId = 0, rowClass = "";
					
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left ">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + lstPaidFrght[i].cus + '</td>'+
											'<td class="text-left">' + lstPaidFrght[i].trnNo + '</td>'+
											'<td class="text-left ">' + lstPaidFrght[i].extFrght + '</td>'+
										'</tr>';
					}
				}
				
				$("#lstPaidFrt tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader4").hide();
			},
			error: function(data){
				$("#mainLoader4").hide();
			}
		});
		   
} /** end of getLstPaidFrght() */
//-----------------------------------------------------------
/** 5. Open Ar */
function getOpenAr(){
		/** It's added here for showing loading animation */
		$("#mainLoader5").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "5",
				param: "",
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			   //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var openArList = arrResponse.openArList;
					var loopLimit = openArList.length;
					var cusId = 0, rowClass = "";
					
					if(loopLimit==0){
						tblRows += '<tr>'+
					      				'<td colspan=3 class="text-left">No Records Found</td>'+
				   				   '</tr>';  	
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left">' + openArList[i].customer + '</td>'+
											'<td class="text-left ">' + openArList[i].amount + '</td>'+			
											'<td class="text-letf ">' + openArList[i].daysPastDue + '</td>'+
										'</tr>';
					}
				}
				$("#openAr tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader5").hide();
			},
			error: function(data){
				$("#mainLoader5").hide();
			}
		});
	    ////console.log(data.errors);
}/** end of getOpenAr() method */
//---------------------------------------------------------
/** 6. New Customer - By Month Rolling 12 month */
function getNewCustomerByMonth(){
      /** It's added here for showing loading animation */
	  $("#mainLoader6").show();
		//calling ajax 
		$.ajax({
			data: {
				execFlg: "6",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var arr1=[];
				var arr2=[];
				var arrMonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var year;
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var newCustomerByMonth = arrResponse.newCustomerByMonth;
					var loopLimit = newCustomerByMonth.length;
					var maxVal=0;
					for( var i=0; i<loopLimit; i++ ){	
							var month=newCustomerByMonth[i].month;        //console.log(month);
							year=newCustomerByMonth[i].year;              //console.log(year);
							var count=newCustomerByMonth[i].count;        //console.log(count);
							maxVal = Math.max(maxVal,count);
							arr1[i]=count;
							arr2[i]=arrMonth[month-1]+"-"+year;				
					}
					
					if(maxVal == 0 || maxVal < 0){
						maxVal = 1;
					}
					
					//console.log(arr1);
					//console.log(arr2);
				}
				
				if (myChart1) {
      			  myChart1.destroy();
     		    }
				
				
			// Bar chart
			myChart1 = new Chart(document.getElementById("customer-count-dashboard"), {
				type: "bar",
				data: {
					labels:arr2,// ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label:"New Customer",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						data: arr1, //[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						barPercentage: .75,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
		 			scales: {
						yAxes: [{
							type: 'linear',
							gridLines: {
								display: true
							},
							stacked: false,
							ticks: {
								stepSize: 20,
								beginAtZero: true,
								max: maxVal
							}
						}],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			});/** End of new chart block */
			
			$("#mainLoader6").hide();
			}, /** ending success block */
			error: function(data){
				$("#mainLoader6").hide();
			}
		}); /** ending ajax block */
	
}/** end of method getNewCustomerByMonth() */
//------------------------------------------------------
/** 7. New Prospects - By Month */
function getNewPropectsCusByMonth(){
		/** It's added here for showing loading animation */
	    $("#mainLoader7").show();
	
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "7",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var arr1=[];
				var arr2=[];
				var arrMonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					var newPropectsCusByMonth = arrResponse.newPropectsCusByMonth;
					var loopLimit = newPropectsCusByMonth.length;
					var maxVal=0;
					for( var i=0; i<loopLimit; i++ ){	
							var month=newPropectsCusByMonth[i].month;    //console.log('Month '+month);
							var year=newPropectsCusByMonth[i].year;      //console.log('year '+year);
							var count=newPropectsCusByMonth[i].count;    //console.log('Count '+count);
							maxVal = Math.max(maxVal,count);
							arr1[i]=count;				
							arr2[i]=arrMonth[month-1]+"-"+year;	
					}
					
					if(maxVal == 0 || maxVal < 0){
						maxVal = 1;
					}
					
					//console.log(arr1);
					//console.log(arr2);
				} /** end of if block */
				
				
				if (myChart2) {
      			  myChart2.destroy();
     		    }
				
				/** started chart block here */
				myChart2 = new Chart(document.getElementById("prospect-count-dashboard"), {
				type: "bar",
				data: {
					labels:arr2,// ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "New Prospect",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						data: arr1, //[54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79],
						barPercentage: .75,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							type: 'linear',
							gridLines: {
								display: true
							},
							stacked: false,
							ticks: {
								stepSize: 10,
								beginAtZero: true,
								max: maxVal
							}
						}],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			}); /** end of new chart block */
				
			/** for hiding loadin icon */
			$("#mainLoader7").hide();	
			
			}, /** ending success block */
			error: function(data){
				$("#mainLoader7").hide();
			}
		}); // ending ajax block
			
}/** end of method getNewPropectsCusByMonth() */

//----------------------------------------------------------
/** 8. Quote vs Orders */
function getQuoteVsOrd(){
	/** It will show the loading animation while loading page */
	$("#mainLoader8").show();
	
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "8",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var arr1=[];
				var arr2=[];
				var arr3=[];
				var arrMonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var maxVal=0,tmpMax1=0,tmpMax2=0;
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var qtVsOrdLst = arrResponse.qtVsOrdLst; 
					var loopLimit = qtVsOrdLst.quoteList.length;   /** fot taking length of quoteList */
					for( var i=0; i<loopLimit; i++ ){	
							var month=qtVsOrdLst.quoteList[i].month;    //console.log('Month '+month);
							var year=qtVsOrdLst.quoteList[i].year;      //console.log('year '+year);
							var count=qtVsOrdLst.quoteList[i].count;    //console.log('Count '+count);
							tmpMax1 = Math.max(tmpMax1,count);
							arr1[i]=count;	
							arr3[i]=arrMonth[month-1]+"-"+year;				
					}
				    loopLimit = qtVsOrdLst.orderList.length; /** fot taking length of orderList */
					for( var i=0; i<loopLimit; i++ ){	
							var month=qtVsOrdLst.orderList[i].month;    //console.log('Month '+month);
							var year=qtVsOrdLst.orderList[i].year;      //console.log('year '+year);
							var count=qtVsOrdLst.orderList[i].count;    //console.log('Count '+count);
							tmpMax2 = Math.max(tmpMax2,count);
							arr2[i]=count;	
							//arr3[i]=arrMonth[month-1]+"-"+year;			
					}
					//console.log('arr1 '+ arr1);
					//console.log('arr2 '+ arr2);
				} /** end of if block */
				
				maxVal = Math.max(tmpMax1,tmpMax2);
				if(maxVal == 0 || maxVal < 0){
						maxVal = 1;
				}
				
				if (myChart3) {
      			  myChart3.destroy();
     		    }
								
				myChart3 = new Chart(document.getElementById("quote-orders-chart"), {
				type: "line",
				data: {
					labels:arr3,//["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Quotes",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						yAxisID: 'A',
						data: arr1, // [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79], 
						barPercentage: .75,
						fill: false,
						categoryPercentage: .5
					}, {
						label: "Orders",
						backgroundColor: "#7e7e7e8a",
						borderColor: "#7e7e7e8a",
						hoverBackgroundColor: "#7e7e7e8a",
						hoverBorderColor: "#7e7e7e8a",
						yAxisID: 'B',
						data: arr2, // [69, 66, 24, 48, 52, 51, 44, 53, 62, 79, 51, 68], 
						barPercentage: .75,
						fill: false,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: true
					},
					tooltips:{   //  It will add thousand comma separator  on over  
           				/*callbacks: {
              				 label: function(tooltipItem, data) {
           						 console.log('in tooltip tooltipItem = '+tooltipItem.index);
             					  var value = data.datasets[0].data[tooltipItem.index];
				                  value = value.toString();
				                  value = value.split(/(?=(?:...)*$)/);
				                  value = value.join(',');
				                  return '$'+value;
               				} 
               			}*/
               			
               			
               			
	         		},
					scales: {
						yAxes: [{
						        id: 'A',
						        type: 'linear',
						        position: 'left',
						        gridLines: {
						    		display: true
								},
								stacked: false,
								ticks: {
									stepSize: 50000,
									beginAtZero: true,
									max: maxVal
								}
						      }, {
						        id: 'B',
						        type: 'linear',
						        position: 'right',
						        gridLines: {
									display: true
								},
								stacked: false,
								ticks: {
									stepSize: 50000,
									beginAtZero: true,
									max: maxVal
								}
						      }],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			});
			/** end of chart block */
				
				
			/** for hiding loadin icon */
			$("#mainLoader8").hide();	
			
			}, /** ending success block */
			error: function(data){
				$("#mainLoader8").hide();
			}
		}); // ending ajax block
	
}/** end of method getQouteVsOrd() */

//-----------------------------------------------------------
/** 9. Quote vs Orders Revenue */
function getQuoteVsOrdRev(){
	/** It will show the loading animation while loading page */
	$("#mainLoader9").show();
	
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "9",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				// console.log(data);
				var arr1=[];
				var arr2=[];
				var arr3=[];
				var arrMonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var maxVal=0,tmpMax1=0,tmpMax2=0;
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var qtVsOrdRevLst = arrResponse.qtVsOrdRevLst; 
					var loopLimit = qtVsOrdRevLst.quoteList.length;   /** fot taking length of quoteList */
					for( var i=0; i<loopLimit; i++ ){	
							var month=qtVsOrdRevLst.quoteList[i].month;    //console.log('Month '+month);
							var year=qtVsOrdRevLst.quoteList[i].year;      //console.log('year '+year);
							var count=qtVsOrdRevLst.quoteList[i].count;    //console.log('Count '+count);
							tmpMax1=Math.max(tmpMax1,count);
							arr1[i]=count;
							arr3[i]=arrMonth[month-1]+"-"+year;				
					}
				    loopLimit = qtVsOrdRevLst.orderRevList.length; /** fot taking length of orderList */
					for( var i=0; i<loopLimit; i++ ){	
							var month=qtVsOrdRevLst.orderRevList[i].month;    //console.log('Month '+month);
							var year=qtVsOrdRevLst.orderRevList[i].year;      //console.log('year '+year);
							var count=qtVsOrdRevLst.orderRevList[i].count;    //console.log('Count '+count);
							tmpMax2=Math.max(tmpMax2,count);
							arr2[i] = count;				
					}
					// console.log('arr1 '+ arr1);
					// console.log('arr2 '+ arr2);
				} /** end of if block */
				
				maxVal=Math.max(tmpMax1,tmpMax2);
				if(maxVal == 0 || maxVal < 0){
						maxVal = 1;
				}
				
				if (myChart4) {
      			  myChart4.destroy();
     		    }
				
				
				/** start here chart block */
				myChart4 = new Chart(document.getElementById("quote-orders-rev-chart"), {
				type: "line",
				data: {
					labels:arr3,//["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Quotes",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						yAxisID: 'A',
						data: arr1,// [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79],
						barPercentage: .75,
						fill: false,
						categoryPercentage: .5
					}, {
						label: "OrdRev",
						backgroundColor: "#7e7e7e8a",
						borderColor: "#7e7e7e8a",
						hoverBackgroundColor: "#7e7e7e8a",
						hoverBorderColor: "#7e7e7e8a",
						yAxisID: 'B',
						data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], //arr2, //
						barPercentage: .75,
						fill: false,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: true
					},	
					tooltips: {   //  It will add thousand comma separator  on tooltips  
					
						/*callbacks: {
              				 label: function(tooltipItem, data) {
           						 console.log('in tooltip tooltipItem = '+tooltipItem.index);
             					  var value = data.datasets[0].data[tooltipItem.index];
				                  value = value.toString();
				                  value = value.split(/(?=(?:...)*$)/);
				                  value = value.join(',');
				                  return '$'+value;
               				} 
               			}*/
               			
               							
			           /*userCallback: function(value, index, values) {
					        value = value.toString();
					        value = value.split(/(?=(?:...)*$)/);
					        value = value.join(',');
					        return '$'+value;
					   }*/
	         		},	
					scales: {
						yAxes: [{
						        id: 'A',
						        type: 'linear',
						        position: 'left',	        
						        gridLines: {
									display: true
								},
								stacked: false,
								ticks: {
									stepSize: 50000,
									beginAtZero: true,
									max: maxVal,
									
									userCallback: function(value, index, values) {
					                	value = value.toString();
					                	value = value.split(/(?=(?:...)*$)/);
					               	 	value = value.join(',');
					                	return '$' + value;
					               }
								}
						      }, {
						        id: 'B',
						        type: 'linear',
						        position: 'right',    
						        gridLines: {
									display: true
								},
								stacked: false,
								ticks: {
									stepSize: 50000,
									beginAtZero: true,
									max: maxVal,
										
					                userCallback: function(value, index, values) {
					                	value = value.toString();
					                	value = value.split(/(?=(?:...)*$)/);
					                	value = value.join(',');
					                	return '$' + value;
					               }	
								},			
							}],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			});
			/** end of chart block */
				
				
			/** for hiding loadin icon */
			$("#mainLoader9").hide();	
			
			}, /** ending success block */
			error: function(data){
				$("#mainLoader9").hide();
			}
		}); // ending ajax block
	
}/** end of method getQuoteVsOrdRev() */

//-----------------------------------------------------------
/** 10. GP Earned by Month */
function getGpErndByMonth(){
	/** It's added here for showing loading animation */
	$("#mainLoader10").show();
	
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "10",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var arr1=[];
				var arr2=[];
				var arrMonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					var gpErndByMnthList = arrResponse.gpErndByMnthList;
					var loopLimit = gpErndByMnthList.length;
					var maxVal=0;
					for( var i=0; i<loopLimit; i++ ){	
							var month=gpErndByMnthList[i].month;    //console.log('Month '+month);
							var year=gpErndByMnthList[i].year;      //console.log('year '+year);
							var gp=gpErndByMnthList[i].gp;          //console.log('gp '+gp);
							maxVal = Math.max(maxVal,gp);
							arr1[i]=gp;
							arr2[i]=arrMonth[month-1]+"-"+year;									
					}
					
					if(maxVal == 0 || maxVal < 0){
						maxVal = 1;
					}
					//console.log(arr1);
				} /** end of if block */
				
				if (myChart5) {
      			  myChart5.destroy();
     		    }
				
				/** chart block started here */
				myChart5=new Chart(document.getElementById("gp-earned-chart"), {
				type: "line",
				data: {
					labels:arr2,// ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "GP Earned",
						/*backgroundColor: "#7e7e7e8a",
						borderColor: "#7e7e7e8a",
						hoverBackgroundColor: "#7e7e7e8a",
						hoverBorderColor: "#7e7e7e8a",*/
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						data:  arr1, // [6900000, 6600000, 2400000, 4800000, 5200000, 510000, 44000000, 53, 62, 79, 51, 68], 
						barPercentage: .75,
						fill: false,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					tooltips: {   /** It will add thousand comma separator  on over  */
			           callbacks: {
			               label: function(tooltipItem, data) {
			                  var value = data.datasets[0].data[tooltipItem.index];
			                  value = value.toString();
			                  value = value.split(/(?=(?:...)*$)/);
			                  value = value.join(',');
			                  return '$'+value;
			               }
			           }
         			}, 
					scales: {
						yAxes: [{	
								ticks: {	
				           		    stepSize: 50000,
				           		    beginAtZero: true,
				           		    max: maxVal,
					           		 // Return an empty string to draw the tick line but hide the tick label
					                // Return `null` or `undefined` to hide the tick line entirely
					                userCallback: function(value, index, values) {
					                	// Convert the number to a string and splite the string every 3 charaters from the end
					                	value = value.toString();
					                	value = value.split(/(?=(?:...)*$)/);
					               	 // Convert the array to a string and format the output
					                	value = value.join(',');
					                	return '$' + value;
					               }					
				                },
						        type: 'linear',
								gridLines: {
									display: true
								},
								stacked: false,
						}],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			}); /** end of chart block */
				
			/** for hiding loadin icon */
			$("#mainLoader10").hide();	
			
			}, /** ending success block */
			error: function(data){
				$("#mainLoader10").hide();
			}
		}); // ending ajax block
				
}/** end of method getGpErndByMonth() */

//-----------------------------------------------------------
/** 11. Quotes by Prospects */
function getQtByPros(){
		$("#mainLoader11").show();
	
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "11",
				param: dateRange,
				slsPer: slsPerson,	
				slpFilter: slpFilter,		
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var arr1=[];
				var arr2=[];
				var arrMonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var qtByPrsList = arrResponse.qtByPrsList;
					var loopLimit = qtByPrsList.length;
					var maxVal=0;
					for( var i=0; i<loopLimit; i++ ){	
							var month=qtByPrsList[i].month;    //console.log('Month '+month);
							var year=qtByPrsList[i].year;      //console.log('year '+year);
							var count=qtByPrsList[i].count;    //console.log('Count '+count);
							maxVal=Math.max(maxVal,count);
							arr1[i]=count;	
							arr2[i]=arrMonth[month-1]+"-"+year;				
							
					}/** end of for loop */
					
					if(maxVal == 0 || maxVal < 0){
						maxVal = 1;
					}
					//console.log(arr1);
				} /** end of if block */
				
				if (myChart6) {
      			  myChart6.destroy();
     		    }
				
				
				/** started chart block here */
				myChart6=new Chart(document.getElementById("quote-prospect-count-chart"), {
				type: "line",
				data: {
					labels:arr2,// ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
					datasets: [{
						label: "Quotes",
						backgroundColor: window.theme.primary,
						borderColor: window.theme.primary,
						hoverBackgroundColor: window.theme.primary,
						hoverBorderColor: window.theme.primary,
						data: arr1,//[69, 66, 24, 48, 52, 51, 44, 53, 62, 79, 51, 68],
						barPercentage: .75,
						fill: false,
						categoryPercentage: .5
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							type: 'linear',
							gridLines: {
								display: true
							},
							stacked: false,
							ticks: {
								stepSize: 10,
								beginAtZero: true,
								max: maxVal
							}
						}],
						xAxes: [{
							stacked: false,
							gridLines: {
								color: "transparent"
							}
						}]
					}
				}
			}); /** end of new chart block */
				
				
			/** for hiding loadin icon */
			$("#mainLoader11").hide();	
			
			}, /** ending success block */
			error: function(data){
				$("#mainLoader11").hide();
			}
		}); /** ending ajax block */	
	
}/** end of method getQtByPros() */
//--------------------------------------------------------
/** 12. Sales by Form */
function getSalesByForm(){
		/** It will show loading animation while data fetching */
		$("#mainLoader12").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "12",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			   //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var salesByFormList = arrResponse.salesByFormList;
					var loopLimit = salesByFormList.length;
					var cusId = 0, rowClass = "";
					
					if(loopLimit==0){
						tblRows += '<tr>'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + salesByFormList[i].customer + '</td>'+
											'<td class="text-left">' + salesByFormList[i].value + '</td>'+
											'<td class="text-left">' + salesByFormList[i].percent +'</td>'+
										'</tr>';
					}/** end of for loop */
					
				}/** end of if block */
				
				$("#salesByForm tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader12").hide();
			},
			error: function(data){
				$("#mainLoader12").hide();
			}
		});/** end of success block */
		
		
}/** end of getSalesByForm()  */
//---------------------------------------------------------
/** 13. Sales by Customer */
function getSalesByCustomer(){
		/** It will show loading animation while data fetching */
		$("#mainLoader13").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "13",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			   //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var salesByCusList = arrResponse.salesByCusList;
					var loopLimit = salesByCusList.length;
					var cusId = 0, rowClass = "";
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + salesByCusList[i].customer + '</td>'+
											'<td class="text-left">' + salesByCusList[i].value + '</td>'+
										'</tr>';
					}/** end of for loop */
					
				}/** end of if block */
				$("#salesByCustomer tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader13").hide();
			},
			error: function(data){
				$("#mainLoader13").hide();
			}
		});		
		
}/** end of getSalesByCustomer() */
//---------------------------------------------------------
/** 14. Average Days to Pay */
function getAvgDaysToPay(){
		/** It will show loading animation while data fetching */
		$("#mainLoader14").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "14",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
			   //console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var avgDaysToPayList = arrResponse.avgDaysToPayList;
					var loopLimit = avgDaysToPayList.length;
					var cusId = 0, rowClass = "";
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + avgDaysToPayList[i].cusId + '</td>'+
											'<td class="text-left">' + avgDaysToPayList[i].avg6 + '</td>'+
											'<td class="text-left">' + avgDaysToPayList[i].avg12 + '</td>'+
										'</tr>';
					}/** end of for loop */
					
				}/** end of if block */
				
				$("#avgDaysToPay tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader14").hide();
			},
			error: function(data){
				$("#mainLoader14").hide();
			}
		});
				
}/** end of getAvgDaysToPay() */
//----------------------------------------------------------
/** 15. Last quote and Last activity */
function getSalesInfoLastQuotes(){
		/** It's added here for showing loading animation */
		$("#mainLoader15").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "15",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var salesInfoLastQuotesList = arrResponse.salesInfoLastQuotesList;
					var loopLimit = salesInfoLastQuotesList.length;
					var cusId = 0, rowClass = "";
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left">' + salesInfoLastQuotesList[i].cusId + '</td>'+
											'<td class="text-left">' + salesInfoLastQuotesList[i].ordNo + '</td>'+
											'<td class="text-left">' + salesInfoLastQuotesList[i].date + '</td>'+
										'</tr>';
					}
				}
				$("#salesLastQuotes tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader15").hide();
			},
			error: function(data){
				$("#mainLoader15").hide();
			}
		});
}/** end of getSalesInfoLastQuotes() */
//---------------------------------------------------------
/** 16. Quote By Date Range */
function getQuoteByDateRange(){
		/** It's added here for showing loading animation */
		$("#mainLoader16").show();
		//console.log('In getQuoteByDateRange '+dateRange);
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "16",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var qtByDtRangeLst = arrResponse.qtByDtRangeLst;
					var loopLimit = qtByDtRangeLst.length;
					var cusId = 0, rowClass = "";
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left">' + qtByDtRangeLst[i].customer + '</td>'+
											'<td class="text-left">' + qtByDtRangeLst[i].ordNo + '</td>'+
											'<td class="text-left">' + qtByDtRangeLst[i].date + '</td>'+
										'</tr>';
					}
				}
				$(".qtByDtRangeLst tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader16").hide();
			},
			error: function(data){
				$("#mainLoader16").hide();
			}
		});
}/** end of getSalesInfoLastQuotes() */
//---------------------------------------------------------
/** 17. Last sales by customer */
function getLstSlsByCus(){
		/** It's added here for showing loading animation */
		$("#mainLoader17").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "17",
				param: "",
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var lstSlsByCusList = arrResponse.lstSlsByCusList;
					var loopLimit = lstSlsByCusList.length;
					var cusId = 0, rowClass = "";
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left">' + lstSlsByCusList[i].customer + '</td>'+
											'<td class="text-left">' + lstSlsByCusList[i].lastSales + '</td>'+
											'<td class="text-left">' + lstSlsByCusList[i].date + '</td>'+
										'</tr>';
					}
				}
				$("#lstSlsByCus tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader17").hide();
			},
			error: function(data){
				$("#mainLoader17").hide();
			}
		});/** end of success block */
		
}/** end of getLstSlsByCus() */
//---------------------------------------------------------
/** 18. Late Invoices */
function getLateInvoice(){
		/** It's added here for showing loading animation */
		$("#mainLoader18").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "18",
				param: dateRange,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var lateInvLst = arrResponse.lateInvLst;
					var loopLimit = lateInvLst.length;
					var cusId = 0, rowClass = "";
					if(loopLimit==0){
						tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + "No Records Found." + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
											'<td class="text-left">' + "" + '</td>'+
									'</tr>';
					}
					
					for( var i=0; i<loopLimit; i++ ){							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td class="text-left ">' + lateInvLst[i].customer + '</td>'+
											'<td class="text-left">' + lateInvLst[i].invNo + '</td>'+
											'<td class="text-left">' + lateInvLst[i].dueDt + '</td>'+
										'</tr>';
					}
				}
				$("#lateInv tbody").html( tblRows );
				
				/** It will hide loading animation once data fetched */
				$("#mainLoader18").hide();
			},
			error: function(data){
				$("#mainLoader18").hide();
			}
		});/** end of success block */
		
}/** end of getLstSlsByCus() */
//---------------------------------------------------------
/** A. this method will call after clicking save button.  */
function updWdgtSel(checkBoxArr){
		  //alert('On clink of save filters');
			
		/** It's added here for showing loading animation */
	   $("#mainLoader").show();
		var arguments = '';
		for(var i=0;i<checkBoxArr.length;i++){
			arguments+=checkBoxArr[i]+',';
		}
		//console.log(checkBoxArr);
		
		//calling ajax 
		$.ajax({
			data: {
				execFlg: "A",
				param: arguments,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var updWgtLst = arrResponse.updWgtLst;
					var loopLimit = updWgtLst.length;
					
					var cusId = 0, rowClass = "";
					for( var i=0; i<loopLimit; i++ ){
						
							var tempSts=updWgtLst[i].status
							var widgetId=updWgtLst[i].widgetId
							
							var status;
							if(tempSts==1){
								if(widgetId==1){ $("#widget1").removeClass("d-none"); getSalesInfoRecentQuotesList();  }
								else if(widgetId==2){ $("#widget2").removeClass("d-none"); getUserNotes(); }
								else if(widgetId==3){ $("#widget3").removeClass("d-none"); getOpenOrd(); }
								else if(widgetId==4){ $("#widget4").removeClass("d-none"); getLstPaidFrght();}
								else if(widgetId==5){ $("#widget5").removeClass("d-none"); getOpenAr(); }
								else if(widgetId==6){ $("#widget6").removeClass("d-none"); getNewCustomerByMonth(); }
								else if(widgetId==7){ $("#widget7").removeClass("d-none"); getNewPropectsCusByMonth(); }
								else if(widgetId==8){ $("#widget8").removeClass("d-none"); getQuoteVsOrd(); }
								else if(widgetId==9){ $("#widget9").removeClass("d-none"); getQuoteVsOrdRev(); }
								else if(widgetId==10){ $("#widget10").removeClass("d-none"); getGpErndByMonth(); }
								else if(widgetId==11){ $("#widget11").removeClass("d-none"); getQtByPros(); }
								else if(widgetId==12){ $("#widget12").removeClass("d-none"); getSalesByForm(); }
								else if(widgetId==13){ $("#widget13").removeClass("d-none"); getSalesByCustomer(); }
								else if(widgetId==14){ $("#widget14").removeClass("d-none"); getAvgDaysToPay(); }
								else if(widgetId==15){ $("#widget15").removeClass("d-none"); getSalesInfoLastQuotes(); }
								else if(widgetId==16){ $("#widget16").removeClass("d-none"); getQuoteByDateRange(); }
								else if(widgetId==17){ $("#widget17").removeClass("d-none"); getLstSlsByCus(); }
								else if(widgetId==18){ $("#widget18").removeClass("d-none"); getLateInvoice(); }
								
								status="checked";
							}
							else{
									if(widgetId==1){ $("#widget1").addClass("d-none");  }
									else if(widgetId==2){ $("#widget2").addClass("d-none"); }
									else if(widgetId==3){ $("#widget3").addClass("d-none"); }
									else if(widgetId==4){ $("#widget4").addClass("d-none"); }
									else if(widgetId==5){ $("#widget5").addClass("d-none"); }
									else if(widgetId==6){ $("#widget6").addClass("d-none"); }
									else if(widgetId==7){ $("#widget7").addClass("d-none"); }
									else if(widgetId==8){ $("#widget8").addClass("d-none"); }
									else if(widgetId==9){ $("#widget9").addClass("d-none"); }
									else if(widgetId==10){ $("#widget10").addClass("d-none"); }
									else if(widgetId==11){ $("#widget11").addClass("d-none"); }
									else if(widgetId==12){ $("#widget12").addClass("d-none"); }
									else if(widgetId==13){ $("#widget13").addClass("d-none"); }
									else if(widgetId==14){ $("#widget14").addClass("d-none"); }
									else if(widgetId==15){ $("#widget15").addClass("d-none"); }
									else if(widgetId==16){ $("#widget16").addClass("d-none"); }
									else if(widgetId==17){ $("#widget17").addClass("d-none"); }
									else if(widgetId==18){ $("#widget18").addClass("d-none"); }
								
								status="unchecked";
							}
							
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td>' + updWgtLst[i].widgetDesc + '</td>' +
											'<td class="text-end"><input class="form-check-input" type="checkbox" value="' + updWgtLst[i].widgetId + '" name="isSelected"' +status+ '></td>'+
										'</tr>';
							
					}
				}/** end of if */
				$("#filtrTbl tbody").html( tblRows );
				
				//It will hide loading animation once data fetched
				$("#mainLoader").hide();
			},
			error: function(data){
				$("#mainLoader").hide();
			}
		});/** ending of ajax block */
				
}/** end of method getValueFromFilter() */		
	
//---------------------------------------------------------
/** B. for adding notes of user */
function addUserNote(){
		//alert('On clink of add Note');
		
		/** It's added here for showing loading animation */
		$("#mainLoader2").show();
		var val = $("#floatingTextarea1").val().trim();
		//console.log(val);
		//calling ajax 
		$.ajax({
			data: {
				execFlg: "B",
				param: val,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				getUserNotes();
				
				//$("#mainLoader2").hide();  /** It will hide loading animation once data fetched  */
			},
			error: function(data){
				$("#mainLoader2").hide();
			}
		});/** ending of ajax block */
		
		
}/** end of method addNote() */	
//-------------------------------------------------------
/** C. For getting list of widget using loginId */
function getWidgetListByUser(){
		/**  It's added here for showing loading animation */
		//$("#mainLoader").show();
		
		/** calling ajax */
		$.ajax({
			data: {
				execFlg: "C",
				param: "",
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				var tblRows = '';
				if (IsJsonString(data.trim())){
					var arrResponse = JSON.parse(data.trim());
					
					var widgetListByUser = arrResponse.widgetListByUser;
					var loopLimit = widgetListByUser.length;
					var flag=0;
					var cusId = 0, rowClass = "";
					for( var i=0; i<loopLimit; i++ ){
						
							var tempSts=widgetListByUser[i].status
							var widgetId=widgetListByUser[i].widgetId
							
							var status;
							if(tempSts==1){
								
								if(widgetId==1){ $("#widget1").removeClass("d-none"); getSalesInfoRecentQuotesList();  }
								else if(widgetId==2){ $("#widget2").removeClass("d-none"); getUserNotes(); }
								else if(widgetId==3){ $("#widget3").removeClass("d-none"); getOpenOrd(); }
								else if(widgetId==4){ $("#widget4").removeClass("d-none"); getLstPaidFrght();}
								else if(widgetId==5){ $("#widget5").removeClass("d-none"); getOpenAr(); }
								else if(widgetId==6){ $("#widget6").removeClass("d-none"); getNewCustomerByMonth(); }
								else if(widgetId==7){ $("#widget7").removeClass("d-none"); getNewPropectsCusByMonth(); }
								else if(widgetId==8){ $("#widget8").removeClass("d-none"); getQuoteVsOrd(); }
								else if(widgetId==9){ $("#widget9").removeClass("d-none"); getQuoteVsOrdRev(); }
								else if(widgetId==10){ $("#widget10").removeClass("d-none"); getGpErndByMonth(); }
								else if(widgetId==11){ $("#widget11").removeClass("d-none"); getQtByPros(); }
								else if(widgetId==12){ $("#widget12").removeClass("d-none"); getSalesByForm(); }
								else if(widgetId==13){ $("#widget13").removeClass("d-none"); getSalesByCustomer(); }
								else if(widgetId==14){ $("#widget14").removeClass("d-none"); getAvgDaysToPay(); }
								else if(widgetId==15){ $("#widget15").removeClass("d-none"); getSalesInfoLastQuotes(); }
								else if(widgetId==16){ $("#widget16").removeClass("d-none"); getQuoteByDateRange(); }
								else if(widgetId==17){ $("#widget17").removeClass("d-none"); getLstSlsByCus(); }
								else if(widgetId==18){ $("#widget18").removeClass("d-none"); getLateInvoice(); }
								
								status="checked";
							}
							else{
									if(widgetId==1){ $("#widget1").addClass("d-none"); }
									else if(widgetId==2){ $("#widget2").addClass("d-none"); }
									else if(widgetId==3){ $("#widget3").addClass("d-none"); }
									else if(widgetId==4){ $("#widget4").addClass("d-none"); }
									else if(widgetId==5){ $("#widget5").addClass("d-none"); }
									else if(widgetId==6){ $("#widget6").addClass("d-none"); }
									else if(widgetId==7){ $("#widget7").addClass("d-none"); }
									else if(widgetId==8){ $("#widget8").addClass("d-none"); }
									else if(widgetId==9){ $("#widget9").addClass("d-none"); }
									else if(widgetId==10){ $("#widget10").addClass("d-none"); }
									else if(widgetId==11){ $("#widget11").addClass("d-none"); }
									else if(widgetId==12){ $("#widget12").addClass("d-none"); }
									else if(widgetId==13){ $("#widget13").addClass("d-none"); }
									else if(widgetId==14){ $("#widget14").addClass("d-none"); }
									else if(widgetId==15){ $("#widget15").addClass("d-none"); }
									else if(widgetId==16){ $("#widget16").addClass("d-none"); }
									else if(widgetId==17){ $("#widget17").addClass("d-none"); }
									else if(widgetId==18){ $("#widget18").addClass("d-none"); }
								status="unchecked";
							}
							tblRows += '<tr class="'+ rowClass +'">'+
											'<td>' + widgetListByUser[i].widgetDesc + '</td>' +
											'<td class="text-end"><input class="form-check-input" type="checkbox" value="' + widgetListByUser[i].widgetId + '" name="isSelected"' +status+ '></td>'+
										'</tr>';
							
					}
				}
				
				$("#filtrTbl tbody").html( tblRows );
				
				
				//It will hide loading animation once data fetched
				$("#mainLoader").hide();
			},
			error: function(data){
				$("#mainLoader").hide();
			}
			
		});/** end of ajax block */
		
} /** end of getWidgetListByUser */
//--------------------------------------------
/** D. for deleting notes of user */
function deleteUserNote(noteId){
	console.log('In deleteUserNote');
		/** It's added here for showing loading animation */
		$("#mainLoader2").show();
		console.log('In method deleteUserNote noteId '+noteId);
		var uId=localStorage.getItem("starAppSlsUserId");
		//calling ajax 
		$.ajax({
			data: {
				execFlg: "D",
				param: noteId,
				slsPer: slsPerson,
				slpFilter: slpFilter,
				usrId: localStorage.getItem("starAppSlsUserId"),
			},
			url: dbUrl,
			type: 'POST',
			dataType: 'text',
			success: function(data){
				getUserNotes();
			},
			error: function(data){
				$("#mainLoader2").hide();
			}
		});/** ending of ajax block */
		
		
}/** end of method deleteUserNote() */	
//--------------------------------------------------------
function getMnthDtFrmToday2(){
	console.log('In getMnthDtFrmToday2');
		
	$.ajax({
		data: {
			execFlg: "MnthBackDt",
			param: "",
			slsPer: "",
			slpFilter: "",
			usrId: localStorage.getItem("starAppSlsUserId"),
		},
		url: dbUrl,
		type: 'POST',
		dataType: 'text',
		success: function(data){
			$("#dtFltr").val(data);
		},
		error: function(data){
			// $("#mainLoader2").hide();
		}
	});/** ending of ajax block */
		
		
}/** end of method deleteUserNote() */	


//--------------------------------------------------------
function getDateRange(){
	$("#dtFltr").removeClass("border-danger");
		var dtFltr = $("#dtFltr").val().trim();
		var frmDt = "-", toDt = "-";
		
		if(dtFltr != "")
		{
			var dtArr = dtFltr.split("-");
			var frmDtArr = dtArr[0].split("/");
			var toDtArr = dtArr[1].split("/");
				  /** year -       month          -      date */
			frmDt = frmDtArr[2].trim()+"-"+frmDtArr[0].trim()+"-"+frmDtArr[1].trim();
			toDt =  toDtArr[2].trim()+"-"+toDtArr[0].trim()+ "-" +toDtArr[1].trim();
		}
		//Month/date/year
		//year-month-date
		//console.log("frmDt "+frmDt);
		//console.log("toDt "+toDt);
		
		//$("#searchResults, .action-btn").hide();
		
		var arguments = frmDt + "/" + toDt;
		
		return arguments;
		
}
/**
 *	This function is validating for valid JSON String
 */
function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

/**
 *	This function is used to allow only numbers in textbox
 */
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateDate(date) {

	var dateMMDDYYYRegex = "^[0-9]{2}/[0-9]{2}/[0-9]{4}$";
    if(date.match(dateMMDDYYYRegex) && isValidDate(date)){	
		return true;
	}
	else{
		return false;
	}
    
    /*var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/ ;
    return dateRegex.test(date);*/
}

function validateTime(time) {
    var timeRegex = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/ ;
    return timeRegex.test(time);
}

function isValidDate(s) {
       var bits = s.split('/');
       var d = new Date(bits[2], bits[0] - 1, bits[1]);
       return d && (d.getMonth() + 1) == bits[0] && d.getDate() == Number(bits[1]);
}
	
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}