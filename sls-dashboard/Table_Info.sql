----------------New Query----------------------------
1. List of recent quotes in total and by customer - (Recent QT - past 10 Days or current month ) (completed) 

select orh_sld_cus_id,cus_cus_long_nm,  COUNT(*) from ortorh_rec, tcttsa_rec,arrcus_rec  where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and cus_cus_id=orh_sld_cus_id and  orh_ord_pfx='QT' and orh_is_slp='RCRU' and tsa_sts_typ='C'  and tsa_ref_itm=0 and tsa_ref_sbitm=0  and to_char (tsa_crtd_dtts, '%Y-%m-%d') >'2022-02-21'  GROUP BY orh_sld_cus_id,cus_cus_long_nm

************2. Last quote, last activity*********

SELECT (orh_sld_cus_id||'-'||cus_cus_long_nm), orh_ord_no, to_char(tsa_lst_upd_dtts, '%d/%m/%Y') from ortorh_rec ,arrcus_rec,tcttsa_rec  
where orh_cmpy_id= tsa_cmpy_id  and orh_sld_cus_id=cus_cus_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no  and tsa_ref_itm=0 and tsa_ref_sbitm=0 and orh_ord_pfx='QT'  and tsa_sts_typ='C' and orh_is_slp='RCRU' ORDER BY tsa_lst_upd_dtts desc  limit 1 ;


**************3. QT by date Range ************

select distinct(ord_ord_no),to_Char(ord_cus_po_dt,'%m/%d/%Y') , (ord_sld_cus_id||'-'|| cus_cus_long_nm ),ord_cus_po_dt from ortord_rec ,arrcus_rec,ortorh_rec where ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id and ord_ord_pfx= orh_ord_pfx  and ord_ord_no= orh_ord_no and ord_ord_itm=0 and ord_ord_pfx='QT' and ord_cus_po_dt is not null and orh_is_slp= 'RCRU' and  ord_cus_po_dt>='2018-01-01' and ord_cus_po_dt <='2022-02-09' order by ord_cus_po_dt desc

Updated one :: 
-------------
select distinct(ord_ord_no),to_Char(ord_cus_po_dt,'%m/%d/%Y') , 
(trim(ord_sld_cus_id)||'-'|| trim(cus_cus_long_nm) ),ord_cus_po_dt from ortorh_rec, ortord_rec ,arrcus_rec 
where orh_cmpy_id = ord_cmpy_id and orh_ord_pfx= ord_ord_pfx and orh_ord_no = ord_ord_no and orh_cmpy_id=cus_cmpy_id and orh_sld_cus_id=cus_cus_id and orh_is_slp= 'AKOG' and ord_cus_po_dt >='2022-05-01' and 
ord_cus_po_dt <='2022-05-23' and orh_ord_pfx='QT' 
and ord_cus_po_dt is not null  order by ord_cus_po_dt desc

************4.No of new customer***************

select month(crd_acct_opn_dt)mon, year(crd_acct_opn_dt)yr, count(distinct cus_cus_long_nm) from  arrcus_rec , arrcrd_rec ,arrshp_rec  where  crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and cus_cus_id= shp_cus_id and shp_is_slp= 'AB' and crd_acct_opn_dt>= '2021-03-01' and crd_acct_opn_dt<= '2022-02-28' and cus_cus_acct_typ<>'P' group by 1,2 order by 2 asc,1 desc


***********5.No of new Propect customer ***********

select month(crd_acct_opn_dt)mon, year(crd_acct_opn_dt)yr, count(distinct cus_cus_long_nm) from  arrcus_rec , arrcrd_rec ,arrshp_rec where  crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and cus_cus_id= shp_cus_id and crd_acct_opn_dt>='2021-03-01' 
and  crd_acct_opn_dt<= '2022-02-28' and cus_cus_acct_typ='P' and shp_is_slp= 'RCRU'
group by 1,2 order by 2 desc,1 desc

************  6.Quote vs Orders ***********
 
Quote::
 
select  count(distinct ord_ord_no), month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr from ortord_rec ,arrcus_rec,arrshp_rec where shp_cmpy_id=cus_cmpy_id  and ord_ord_pfx= orh_ord_pfx  and ord_ord_no= orh_ord_no and ord_ord_itm=0  and ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id where ord_ord_pfx='QT' and ord_cus_po_dt is not null and orh_is_slp='RCRU' and ord_ord_itm= 0 and ord_cus_po_dt>='2021-01-01' and ord_cus_po_dt <='2022-01-31'group by 2,3   order by 3 desc ,2  desc


Orders::

select  count(distinct stn_ord_no), month(stn_cus_po_dt)mon , year(stn_cus_po_dt)yr from sahstn_rec where stn_ord_pfx='SO' and stn_cus_po_dt is not null and stn_is_slp= 'RCRU' and  stn_cus_po_dt>='2021-01-01' 
and stn_cus_po_dt <='2022-01-31'group by 2,3   order by 3 desc ,2  desc



select  count(distinct ord_ord_no), month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr from ortord_rec ,arrcus_rec,arrshp_rec,ortorh_rec
where shp_cmpy_id=cus_cmpy_id  and ord_ord_pfx= orh_ord_pfx  and ord_ord_no= orh_ord_no and ord_ord_itm=0 
and ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id and ord_ord_pfx='QT'
and ord_cus_po_dt is not null and orh_is_slp='RCRU' and ord_ord_itm= 0 
and ord_cus_po_dt>='2021-01-01' and ord_cus_po_dt <='2022-01-31'group by 2,3 order by 3 desc ,2 desc

****************Quote vs Orders Revenue *******
Quote :: 


Order Revenue :: 

select  sum(stn_tot_val), month(stn_cus_po_dt)mon , year(stn_cus_po_dt)yr from sahstn_rec where stn_ord_pfx='SO' and stn_cus_po_dt is not null   and  stn_is_slp= 'AM'  and stn_cus_po_dt>= '2022-05-01'and stn_cus_po_dt <='2022-05-23'  and stn_cus_po_dt>= '2021-06-01'and stn_cus_po_dt <='2022-05-31'  group by 2,3   order by 3 asc ,2  asc
 
************** 7.Sales by customer *****************

SELECT (stn_sld_cus_id || '-'||stn_nm1), round(sum(stn_tot_val),2)Sales FROM sahstn_rec  where  stn_is_slp= 'RCRU' and  stn_inv_dt>'2010-01-01'and stn_inv_dt< '2022-01-31' group by 1 order by 2 desc  

***************** 8. Sales by form ******************

select stn_frm, frm_desc25 , round(sum(stn_tot_val),2)Sales FROM sahstn_rec, inrfrm_rec  WHERE stn_frm= frm_frm and stn_is_slp= 'RCRU' and  stn_inv_dt>'2010-01-01'
and stn_inv_dt< '2022-01-31' group by 1,2 order by 2 desc limit 25

***************** 9. Gp earned for R12M *************

select month(stn_inv_dt) ,year(stn_inv_dt), round((sum(stn_tot_val)-sum(stn_tot_avg_val)),2) gross_profit FROM sahstn_rec where stn_is_slp= 'RCRU' and  stn_inv_dt> '2021-03-01' and stn_inv_dt< '2022-02-28' and stn_inv_dt is not null  group by 1,2 order by 2 asc , 1 asc

************* 10.Quotes by prospect ******************

select month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr ,count(distinct ord_ord_no)
from ortord_rec ,arrcus_rec,arrcrd_rec, arrshp_rec where ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id and crd_cus_id=cus_cus_id and cus_cus_id= shp_cus_id and ord_ord_pfx='QT' and cus_cus_acct_typ='P' and ord_cus_po_dt is not null and shp_is_slp= 'RCRU' and ord_cus_po_dt>='2021-01-01' and ord_cus_po_dt <='2022-02-18' group by 1,2 order by 2 desc ,1 desc

************ 11. Last sales by customer **********

select  coc_lst_sls_dt ,coc_lst_pmt_amt ,(coc_cus_id||'-'|| cus_cus_long_nm)  from arbcoc_rec, arrcus_rec,arrshp_rec where coc_cus_id= cus_cus_id and cus_cus_id= shp_cus_id
and shp_is_slp= 'RCRU' order by coc_lst_sls_dt desc 

******* 12. Open AR  (Done) ***************

select rvh_upd_ref, to_Char(rvh_due_dt,'%Y-%m-%d'), rvh_balamt , (rvh_sld_cus_id||'-'|| stn_nm1)  from artrvh_rec,  sahstn_rec  where  rvh_balamt<> 0 and rvh_cmpy_id=stn_cmpy_id and  rvh_upd_ref=stn_upd_ref and  stn_is_slp= 'CBIR' order by rvh_balamt desc 



********* 13. Average Days to Pay (Done)***********

select (cub_cus_id||'-'||cus_cus_long_nm),  cub_apd_1,cub_apd_2,cub_apd_3,cub_apd_4,cub_apd_5,cub_apd_6,cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12 from arbcub_rec,arrcus_rec,arrshp_rec where cub_cus_id=cus_cus_id and cub_cus_id=shp_cus_id and shp_is_slp= 'RCRU' ;

*********** 14. Open Orders (Done) **********

select (cus_cus_long_nm ||'-'|| orh_sld_cus_id),orh_ord_no , oit_orig_ord_val  from ortorh_rec, tcttsa_rec,arrcus_rec, ortoit_rec    where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and cus_cus_id= orh_sld_cus_id and orh_ord_pfx= oit_ref_pfx and orh_ord_no = oit_ref_no and oit_ref_itm=0 and  orh_ord_pfx='SO'  and tsa_sts_actn<>'C' and tsa_sts_typ= 'T' and tsa_ref_itm=0 and tsa_ref_sbitm =0  and to_char (tsa_crtd_dtts, '%Y-%m-%d') >'2022-03-28'  order by oit_orig_ord_val desc,   tsa_crtd_dtts desc


****************15. Late invoice.*************

select (rvh_sld_cus_id||'-'||cus_cus_long_nm),rvh_upd_ref, to_Char(rvh_due_dt,'%m/%d/%Y')  from artrvh_rec,arrcus_rec,ortorh_rec  where rvh_sld_cus_id = cus_cus_id and rvh_ord_pfx= orh_ord_pfx and rvh_ord_no= orh_ord_no and  orh_is_slp= 'RCRU' and rvh_due_dt <'2022-03-09' order by rvh_due_dt desc limit 100;

Updated :: 

select (trim(rvh_sld_cus_id)||'-'||trim(cus_cus_long_nm))cus,rvh_upd_ref, to_Char(rvh_due_dt,'%m/%d/%Y')rvh_due_dt,rvh_inv_dt from artrvh_rec ,arrcus_rec,arrshp_rec where rvh_cmpy_id = cus_cmpy_id and rvh_sld_cus_id = cus_cus_id and shp_cmpy_id = rvh_cmpy_id  and shp_cus_id = rvh_cr_ctl_cus_id  and  shp_shp_to = 0 and shp_is_slp='AKOG' and rvh_balamt >0 and to_Char(rvh_due_dt,'%Y-%m-%d') >'2022-05-01' and  to_Char(rvh_due_dt,'%Y-%m-%d') <'2022-06-23' and rvh_due_dt < today order by rvh_due_dt desc 



*********16. Last Paid Freight Information **********

select (stn_sld_cus_id||'-'|| stn_nm1),stn_upd_ref,stn_shpt_pfx,stn_shpt_no from sahstn_rec where stn_tot_repl_val <> 0 and stn_is_slp= 'RCRU'  order by stn_upd_cy desc , stn_upd_mth desc , stn_upd_dy desc limit 1;

Internal 

select sum(csi_bas_cry_val) from cttcsi_rec WHERE csi_ref_pfx='SI' AND csi_ref_no= '215876' AND csi_ref_itm='999';

External
select sum(csi_bas_cry_val) from cttcsi_rec WHERE csi_ref_pfx='SI' AND csi_ref_no= '215876' and  csi_cst_no=3 AND csi_cst_cl='I'; 





-----------------------------------------------------------
1. List of recent quotes in total and by customer - (Recent QT - past 10 Days or current month ) (completed) 

select orh_sld_cus_id, COUNT(*)
from ortorh_rec, tcttsa_rec  where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no   and  orh_ord_pfx='QT'  and tsa_sts_typ='C'  and to_char (tsa_crtd_dtts, '%Y-%m-%d') > '2021-02-01'
GROUP BY orh_sld_cus_id 
   
1. List of recent quotes in total and by customer - (Recent QT - past 10 Days or current month ) (completed) 

select orh_sld_cus_id,cus_cus_long_nm,  COUNT(*) from ortorh_rec, tcttsa_rec,arrcus_rec  where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and cus_cus_id=orh_sld_cus_id and  orh_ord_pfx='QT'  and tsa_sts_typ='C'  and tsa_ref_itm=0 and tsa_ref_sbitm=0  and to_char (tsa_crtd_dtts, '%Y-%m-%d') >'2022-02-21'  GROUP BY orh_sld_cus_id,cus_cus_long_nm


2. Last quote, last activity  (completed)

SELECT (orh_sld_cus_id||'-'||cus_cus_long_nm), orh_ord_no, to_char(tsa_lst_upd_dtts, '%d/%m/%Y') from ortorh_rec ,arrcus_rec,tcttsa_rec  
where orh_cmpy_id= tsa_cmpy_id  and orh_sld_cus_id=cus_cus_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no  and tsa_ref_itm=0 and tsa_ref_sbitm=0 and orh_ord_pfx='QT'  and tsa_sts_typ='C'    ORDER BY tsa_lst_upd_dtts desc  limit 1 ;

					
		
3. QT by date Range 

select   distinct(ord_ord_no),to_Char(ord_cus_po_dt,'%m/%d/%Y') , (ord_sld_cus_id||'-'|| cus_cus_long_nm ),ord_cus_po_dt from ortord_rec ,arrcus_rec
where ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id and ord_ord_pfx='QT' and ord_cus_po_dt is not null and ord_cus_po_dt>='2018-01-01' and ord_cus_po_dt <='2022-02-09' order by ord_cus_po_dt desc




4.No of new customer for Rolling 12 Mont (completed)

select month(crd_acct_opn_dt)mon, year(crd_acct_opn_dt)yr, count(distinct cus_cus_long_nm) from  
arrcus_rec , arrcrd_rec where  crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and crd_acct_opn_dt>='2021-02-15' and 
crd_acct_opn_dt<= '2022-02-15' and cus_cus_acct_typ<>'P'
group by 1,2 order by 2 desc,1 desc 

5.No of new Propect customer (Completed)

select month(crd_acct_opn_dt)mon, year(crd_acct_opn_dt)yr, count(distinct cus_cus_long_nm) from  
arrcus_rec , arrcrd_rec where  crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and crd_acct_opn_dt>='2021-03-01' 
and  crd_acct_opn_dt<= '2022-02-28' and cus_cus_acct_typ='P'
group by 1,2 order by 2 desc,1 desc

6.Quote vs Orders
 
select   count(distinct ord_ord_no), month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr from ortord_rec 
where ord_ord_pfx='QT' and ord_cus_po_dt is not null and ord_cus_po_dt>='2021-01-01' 
and ord_cus_po_dt <='2022-01-31'group by 2,3   order by 3 desc ,2  desc

select  count(distinct ord_ord_no), month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr from ortord_rec 
where ord_ord_pfx='SO' and ord_cus_po_dt is not null and ord_cus_po_dt>='2021-01-01' 
and ord_cus_po_dt <='2022-01-31'group by 2,3   order by 3 desc ,2  desc


7.Sales by customer

SELECT (cus_cus_id || '-'||cus_cus_long_nm), round(sum(stn_tot_val),2)Sales FROM sahstn_rec, arrcus_rec WHERE cus_cus_id=stn_sld_cus_id and stn_inv_dt>'2010-01-01'
and stn_inv_dt< '2022-01-31' group by 1 order by 2 desc limit 25 

8. Sales by form 

select stn_frm, frm_desc25 , round(sum(stn_tot_val),2)Sales FROM sahstn_rec, inrfrm_rec  WHERE stn_frm= frm_frm and stn_inv_dt>'2010-01-01'
and stn_inv_dt< '2022-01-31' group by 1,2 order by 2 desc limit 25 

9. Gp earned for R12M
select month(stn_inv_dt) ,year(stn_inv_dt), round((sum(stn_tot_val)-sum(stn_tot_avg_val)),2) gross_profit FROM sahstn_rec where  stn_inv_dt> '2021-03-01' and stn_inv_dt< '2022-02-28'  group by 1,2 order by 2 asc , 1 asc
 

10. Quotes by prospect

select month(ord_cus_po_dt)mon , year(ord_cus_po_dt)yr ,count(distinct ord_ord_no)
from ortord_rec ,arrcus_rec,arrcrd_rec where ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id and crd_cus_id=cus_cus_id and ord_ord_pfx='QT' and cus_cus_acct_typ='P' and ord_cus_po_dt is not null and ord_cus_po_dt>='2021-01-01' 
and ord_cus_po_dt <='2022-02-18' group by 1,2   order by 2 desc ,1  desc

11. Last sales by customer 
select  coc_lst_sls_dt ,coc_lst_pmt_amt ,(coc_cus_id||'-'|| cus_cus_long_nm)  from arbcoc_rec, arrcus_rec where coc_cus_id= cus_cus_id order by coc_lst_sls_dt desc 

12. Open AR 
select rvh_upd_ref, rvh_due_dt, rvh_balamt , (rvh_sld_cus_id||'-'|| cus_cus_long_nm)  from artrvh_rec, arrcus_rec  where rvh_sld_cus_id=cus_cus_id  and rvh_balamt<> 0 order by rvh_due_dt asc  limit 25 

13. Average Days to Pay 

select (cub_cus_id||'-'||cus_cus_long_nm),  cub_apd_1,cub_apd_2,cub_apd_3,cub_apd_4,cub_apd_5,cub_apd_6,cub_apd_7, cub_apd_8, cub_apd_9, cub_apd_10, cub_apd_11, cub_apd_12 from arbcub_rec,arrcus_rec where cub_cus_id=cus_cus_id;
 

14. Open Orders 

select (orh_sld_cus_id ||'-'||cus_cus_long_nm),orh_ord_no 
from ortorh_rec, tcttsa_rec,arrcus_rec   where orh_cmpy_id= tsa_cmpy_id and orh_ord_pfx= tsa_ref_pfx and orh_ord_no = tsa_ref_no and cus_cus_id= orh_sld_cus_id and  orh_ord_pfx='SO'  and tsa_sts_actn<>'C' and tsa_sts_typ= 'T' and tsa_ref_itm=0 and tsa_ref_sbitm =0 and to_char(tsa_crtd_dtts, '%Y-%m-%d') > '2022-02-01' and to_char(tsa_crtd_dtts, '%Y-%m-%d')< '2022-03-01' limit 10


15. Late invoice.

select (rvh_sld_cus_id||'-'||cus_cus_long_nm),rvh_upd_ref, to_Char(rvh_due_dt,'%m/%d/%Y')  from artrvh_rec,arrcus_rec  where rvh_sld_cus_id = cus_cus_id and rvh_due_dt <'2022-03-09' order by rvh_due_dt desc limit 100;


16. Last Paid Freight Information

select (stn_sld_cus_id||'-'|| stn_nm1),stn_upd_ref,stn_shpt_pfx,stn_shpt_no from sahstn_rec where stn_tot_repl_val <> 0 order by stn_upd_cy desc , stn_upd_mth desc , stn_upd_dy desc limit 1;

Internal 

select sum(csi_bas_cry_val) from cttcsi_rec WHERE csi_ref_pfx='SI' AND csi_ref_no= '215876' AND csi_ref_itm='999';

External
select sum(csi_bas_cry_val) from cttcsi_rec WHERE csi_ref_pfx='SI' AND csi_ref_no= '215876' and  csi_cst_no=3 AND csi_cst_cl='I'; 


17.


/*--To get Top Customer details based on Year--*/

SELECT stn_is_slp, COUNT(distinct stn_ord_no) invoiced_order_count,year(stn_inv_dt),round(sum(stn_tot_val), 0) Total_Revenue,round(sum(stn_tot_val) / 12, 0) Monthly_Revenue,round((sum(stn_tot_val) - sum(stn_tot_avg_val)), 2) total_gross_profit,round((sum(stn_tot_val) - sum(stn_tot_avg_val)) / 12, 2) Monthly_Gross_Profit FROM sahstn_rec WHERE 1 = 1	AND year(stn_inv_dt)=2022 and stn_inv_dt is not null and stn_is_slp='JCAI' group by stn_is_slp,3 having sum(stn_tot_val)<>0 order by 2,1; 


dAvgOrder=dTotalRevenue/iInvoicedOrders;




/*--Get Total Prospects Counts on Given Year Basis and SalesPerson name--*/

select count(distinct cus_cus_long_nm)prospects,year(crd_acct_opn_dt),shp_is_slp	from	arrcrd_rec,	arrcus_rec,	arrshp_rec	where	shp_cmpy_id=cus_cmpy_id and	shp_cus_id=cus_cus_id  and	crd_cmpy_id=cus_cmpy_id 	and crd_cus_id=cus_cus_id	AND year(crd_acct_opn_dt)=2022 and shp_is_slp='JCAI' group by 2,3 order by 2,1  


/*--Get Total Quotes Counts on Given Year Basis and SalesPerson name--*/

select count(distinct ord_ord_no)quotes,year(ord_cus_po_dt),shp_is_slp from ortord_rec ,arrcus_rec,arrshp_rec where shp_cmpy_id=cus_cmpy_id and shp_cus_id=cus_cus_id  and ord_cmpy_id=cus_cmpy_id and ord_sld_cus_id=cus_cus_id AND year(ord_cus_po_dt)=2022 and ord_ord_pfx='QT' and ord_cus_po_dt is not null and shp_is_slp='JCAI' group by 2,3 order by 2,1;


/*--To get Cus Sold --*/
select count(distinct cus_cus_id)	from arrcrd_rec,arrcus_rec,	arrshp_rec	where	shp_cmpy_id=cus_cmpy_id and	shp_cus_id=cus_cus_id  and	crd_cmpy_id=cus_cmpy_id and crd_cus_id=cus_cus_id and year(crd_co_lim_eff_dt)=2022 and shp_is_slp='JCAI'



**************** Custom table *********************

create  TABLE slsdsh_usr_notes (
USR_LGN_ID   char(8) NOT NULL,
Note_ID      serial  NOT NULL , 
USR_NOTES  varchar(255),
USR_NOTE_DT   datetime year to second
);


create  TABLE slsdsh_wdgt (
WDGT_ID   integer  NOT NULL , 
WDGT_DESC  varchar(255), 
PRIMARY KEY (WDGT_ID)
);

create  TABLE slsdsh_wdgtDtls (
USR_LGN_ID   char(8) NOT NULL, 
WDGT_ID      integer NOT NULL,
status       integer NOT NULL ,
FOREIGN KEY (WDGT_ID) REFERENCES slsdsh_wdgt(WDGT_ID)
);

create table slsdsh_usrDtls(
lgn_id char(8) NOT NULL,
usr_role char(1) NOT NULL
);

select lgn_id from slsdsh_usrDtls where lgn_id='Binayak1' and usr_role='A'

drop table slsdsh_usrDtls;

insert into slsdsh_usrDtls values("albertos","A");
insert into slsdsh_usrDtls values("kkoehler","A");
insert into slsdsh_usrDtls values("arheinne","A");

insert into slsdsh_usrDtls values("srussell","A");
insert into slsdsh_usrDtls values("mdanis","A");
insert into slsdsh_usrDtls values("consult","A");

insert into slsdsh_usrDtls values("jpazderk","A");
insert into slsdsh_usrDtls values("rsch","A");

delete from slsdsh_usrDtls where lgn_id = "jpazderk";
delete from slsdsh_usrDtls where lgn_id = "rsch";

select * from slsdsh_usrDtls;

insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(1,"List of Recent Quotes");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(2,"To Do List");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(3,"Open Orders by Req Date");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(4,"Last Paid Freight Information");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(5,"Open AR");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(6,"New Customer - By Month");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(7,"New Prospects - By Month");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(8,"Quote vs Orders Count");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(9,"Quote vs Orders Revenue");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(10,"GP Earned by Month");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(11,"Quotes by Prospects");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(12,"Sales by Form");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(13,"Sales by Customer");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(14,"Average Days to Pay");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(15,"Last Quote and Activity");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(16,"Quote By Date Range");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(17,"Last Sales by Customer");
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(18,"Late Invoices");
-----------------------------------------------------------
insert into slsdsh_usr_dtls(USR_LGN_ID,USR_COMMNT,USR_COM_DT) values(?,?,?);
insert into slsdsh_usr_dtls(USR_LGN_ID,USR_COMMNT,USR_COM_DT) values("Nawab123","For testing","Test comment");


insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(?,?);
insert into slsdsh_wdgt(WDGT_ID,WDGT_DESC) values(?,?);

insert into slsdsh_wdgtDtls(USR_LGN_ID,WDGT_ID,status) values(?,?,?);

select USR_LGN_ID,WDGT_ID, status from slsdsh_usr_dtls as usr,slsdsh_wdgtDtls wdgt where usr.USR_LGN_ID= wdgt.USR_LGN_ID


select usr.USR_LGN_ID,WDGT_ID, status from slsdsh_usr_dtls as usr,slsdsh_wdgtDtls wdgt where usr.USR_LGN_ID= wdgt.USR_LGN_ID 

delete from slsdsh_usr_notes where USR_LGN_ID=? and Note_ID=?;
